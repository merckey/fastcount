#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <malloc.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <stack>
#include <vector>
#include <hash_map>
#include <map>
#include <set>

class GeneCounter{

private:
	std::vector<std::string> geneids;
	std::vector<float> gcounts;
	int unassigned_rcounts;
	
public:
	
	map<int, vector<int>> informativeId2Genes;
	GeneCount_Info()
	{
		unassignedReadCount = 0;
	}

	int return_geneTotalNum()
	{
		return geneIdVec.size();
	}

	int return_inforKmerTotalNum()
	{
		return informativeId2Genes.size();
	}
	string return_geneIdStr(int tmp)
	{
		return geneIdVec[tmp-1];
	}

	vector<string> split(string str, char delimiter) {
		vector<string> internal;
		stringstream ss(str); // Turn the string into a stream.
		string tok;
	
		while(getline(ss, tok, delimiter)) {
			internal.push_back(tok);
		}

		return internal;
	}
	// parse index to Genes indices map
	void init_informativeId2Genes(string& repKmer2genesFile)
	{
		ifstream ifs(repKmer2genesFile.c_str());
		string tmpStr;
		vector<string> sep1;
		vector<string> sep2;
		
		while(!ifs.eof())
		{
			
			vector<int> tmpVec;
			getline(ifs, tmpStr);
			if(tmpStr == "")
				break;
			sep1 = split(tmpStr,'\t');
			sep2 = split(sep1[1],',');
			for(int i=0; i < sep2.size(); i++){
				tmpVec.push_back(atoi(sep2[i].c_str()));
			}
			informativeId2Genes.insert(pair<int,vector<int>>(atoi(sep1[0].c_str()),tmpVec));
		}
		ifs.close();
	}
	// parse gene index file
	void initaite_gene2reissuedFile(string& geneIdListFile)
	{
		ifstream geneInfo_ifs(geneIdListFile.c_str());
		while(!geneInfo_ifs.eof())
		{
			string tmpStr;
			getline(geneInfo_ifs, tmpStr);
			if(tmpStr == "")
				break;
			geneIdVec.push_back(tmpStr);
			geneCountVec.push_back(0);
		}
		geneInfo_ifs.close();
	}
	/*
		void addGeneCount(vector <int> tmpGeneReissuedId)
	{
		float add = 1.0/tmpGeneReissuedId.size()
		for ( auto p:tmpGeneReissuedId){
			geneCountVec[p-1] = geneCountVec[p-1]+add
		}
	}
	*/
	void addGeneCount(int tmpGeneReissuedId, float add)
	{
		int tmpGeneIndex = tmpGeneReissuedId - 1;
		geneCountVec[tmpGeneIndex] = geneCountVec[tmpGeneIndex] + add;
	}

	void addUnassignedReadCount()
	{
		unassignedReadCount ++;
	}

	void printGeneCountInfo(string& tmpOutputFile)
	{
		ofstream geneCount_ofs(tmpOutputFile.c_str());
		for(int tmp = 0; tmp < geneCountVec.size(); tmp++)
			geneCount_ofs << geneIdVec[tmp] << "\t" << geneCountVec[tmp] << endl;
		//geneCount_ofs << endl << "Unassigned read #:\t" << unassignedReadCount << endl;
		geneCount_ofs.close();
	}

	void printAssignStatsInfo(string& tmpOutputFile)
	{
		ofstream assignStats_ofs(tmpOutputFile.c_str());
		int assignedReadCout_total = 0;
		for(int tmp = 0; tmp < geneCountVec.size(); tmp++)
			assignedReadCout_total += geneCountVec[tmp];
		assignStats_ofs << "Assigned read #:\t" << assignedReadCout_total << endl;
		assignStats_ofs << "Unassigned read #:\t" << unassignedReadCount << endl;
		assignStats_ofs.close();
	}


};
#endif