#pragma once
#include "OthelloIndex.h"


struct Cell{
public:
	//keyT id; // split id to cb and umi?
	valueT gid;
	keyT cb;
	//int count = 0;
 //    void splitgrp(const keyType &key, uint32_t &grp, keyType &keyInGroup) {
	//     int mvcnt = 2 * kmerlength - splitbit;
	//     keyType high = (key >> mvcnt);
	//     grp = high;
	//     keyType lowmask = 1;
	//     lowmask <<= mvcnt;
	//     keyInGroup = (key & (lowmask-1));
	// }

	bool operator<(const Cell& l ) const 
{
   // This is fine for a small number of members.
   // But I prefer the brute force approach when you start to get lots of members.
   return ( l.cb < cb ) ||
          (( l.cb == cb) && ( l.gid < gid ));
}
	// bool operator < (const Cell& cell_t) const{
	// 	return cell_t.cb < cb;
	// }
	//    return ( l.a < r.a ) ||
 //          (( l.a == r.a) && ( l.b < r.b ))
};



class GeneCounter{
public:
	GeneCounter(OthelloIndex &index,Opts &opt): index(index),
		counts(index.num_genes, 0),opt(opt),k(opt.k){};
	std::vector<double> counts;
	OthelloIndex &index;
	Opts &opt;
	int k;
	void incrCounts(valueT g);
};

struct GeneClass{
	bool isfusion;
	valueT left;
	valueT right;
};

/* 0-based postion */
struct GeneLog{
	GeneLog(){};
	GeneLog(valueT value,int last_pos);
	valueT gv;
	int tot;
	int maxBlock;
	int currBlock;
	int first_p;
	int last_p;
	int n_mismatch;
	int n_rep;
	std::vector<int> pos;
	/* Check whether continuity of kmers
		Update the block size of the gene
	*/
	bool checkCont(int pos,int stride){
		if(pos == last_p+stride){
			currBlock++;
			last_p = pos;
			if(currBlock > maxBlock){
				maxBlock = currBlock;
			}
			return true;
		}else{
			last_p = pos;
			currBlock = 1;
			return false;
		}
	}
	bool estMis(int stride, int max, int k){
		return (last_p - first_p + 1 - tot - n_rep - k <= max);
	}
	bool operator<(const GeneLog& a) const
    {
        return tot > a.tot;
    }
};
