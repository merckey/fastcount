#include "KmerCreater.h"

KmerStatus::KmerStatus(valueT &v){
  genes.insert(v);
  size=1;

}
void KmerStatus::update(valueT &v){
  if (size < 3){
    genes.insert(v);
    size = genes.size();

  }
  
}
unsigned char KmerCreater::reverseBitsInByte(unsigned char v){  
  return (v * 0x0202020202ULL & 0x010884422010ULL) % 1023;
}

keyT KmerCreater::reverseComplement(keyT key) {
    //std::cerr << k << std::endl;
    if (key & 0x8000000000000000ULL){
        return key;
    }
    keyT ans = 0ULL;
    for (uint32_t i = 0 ; i < sizeof(keyT); i++) {
        ans <<=8;
        ans |= reverseBitsInByte( (unsigned char) key);
        key>>=8;
    }
    keyT qq = ((~ (ans ^ (ans>>1))) & 0x5555555555555555ULL);
    ans = (ans^(qq*3));
    ans >>= (sizeof(keyT)*8 - k *2);

    return ans;
}

keyT KmerCreater::minSelfAndRevcomp(const keyT key) {
	//std::cout << "before" << k << std::endl;
    keyT comp = reverseComplement(key);
    return (comp < key)? comp : key;
}
void binary(int t){
	std::cout << std::bitset<8>(t).to_string<char>() << std::endl;
}

/*  Retrieve the next key
    pos: last jump position
         pos will be updated by stride.
*/

keyT KmerCreater::NextKey(keyT &lkey, const char* s, int& pos,int move){
    //pos = pos + stride;
    keyT nextNc;
    if(move == 1){
        nextNc = charToKey[*(s + pos + k) - 'A'];     
    }else{
        convert(s+pos+k, nextNc, move);
    }
    pos = pos + move;

    if(move < k){
        lkey <<= (2*move);
        lkey = (lkey|nextNc)&mask;
    }else{
        lkey = nextNc; 
    }
    //return minSelfAndRevcomp(lkey);
    return lkey;
}

keyT KmerCreater::NextKey(keyT &lkey, const char* s, int& pos){
    //pos = pos + stride;
    keyT nextNc;
    if(stride == 1){
        nextNc = charToKey[*(s + pos + k) - 'A'];     
    }else{
        convert(s+pos+k, nextNc, stride);
    }
    pos = pos + stride;

    if(stride < k){
        lkey <<= (2*stride);
        lkey = (lkey|nextNc)&mask;
    }else{
        lkey = nextNc; 
    }
    //return minSelfAndRevcomp(lkey);
    return lkey;
}


bool KmerCreater::convert(const char *s, keyT &key, int len) {
    switch (*s) {
    case 'A':
    case 'T':
    case 'G':
    case 'C':
        key = 0;
        int l = 0;
        //binary(key);
        while (*s == 'A' || *s == 'C' || *s =='T' || *s =='G' || *s == 'N') { //A = 0, C == 1, G == 2, T == 3, N:keymer = 0x8000000000000000ULL;
            key <<=2;
            //binary(key);
            if (*s == 'N'){
                key = -1;
                //std::cerr << "is N" << std::endl;
                return false;//0x8000000000000000ULL;
            }
            switch (*s) {
            case 'T':
                key++;
                //binary(key);
 
            case 'G':
                key++;
                //binary(key);

            case 'C':
                key++;
                //binary(key);
            }
            s++;
            l++;
            if (l==len){
            	return true;
            }
        }
        return true;

    }
    key=-1;
    return false;

}
bool KmerCreater::convert(const char *s, keyT &key ) {
    switch (*s) {
    case 'A':
    case 'T':
    case 'G':
    case 'C':
        key = 0;
        int l = 0;
        //binary(key);
        while (*s == 'A' || *s == 'C' || *s =='T' || *s =='G' || *s == 'N') { //A = 0, C == 1, G == 2, T == 3, N:keymer = 0x8000000000000000ULL;
            key <<=2;
            //binary(key);
            if (*s == 'N'){
                key = -1;
                //std::cerr << "is N" << std::endl;
                return false;//0x8000000000000000ULL;
            }
            switch (*s) {
            case 'T':
                key++;
                //binary(key);
 
            case 'G':
                key++;
                //binary(key);

            case 'C':
                key++;
                //binary(key);
            }
            s++;
            l++;
            if (l==k){
                return true;
            }
        }
        return true;

    }
    key=-1;
    return false;

}

/*
in: seq
out: vector of minK
*/

void KmerCreater::extractKmer(const char *s, const int l)
{
	// Not implement jump in kmer extraction
	int jump_length = 1; 
	int kmer_start = 0;
    int skip = 0;
	keyT mask = bitmask();
	keyT kmer=-1;
    keyT kNew;
    bool b;
    while(kmer_start + k - 1 < l){
        if(kmer == -1){

            b = convert(s+kmer_start,kmer);
            if(b){                
                //kmers.push_back(minSelfAndRevcomp(kmer));
                kmers.push_back((kmer));
                
            }
        }else{
            char c = s[kmer_start - 1 + k];
            
            if (c == 'N'){
                kmer = -1;
            }else{
                kmer <<= 2;
                kNew = charToKey[*(s + kmer_start - 1 + k) - 'A'];
                kmer = (kmer|kNew)&mask;
                //kmers.push_back(minSelfAndRevcomp(kmer));
                kmers.push_back((kmer));
            }
        }
        kmer_start++;

    }
    /*while(!convert(s + kmer_start, kmer) && kmer_start+k < l){
        kmer_start++;
    }
    std::cerr << kmer_start << std::endl;
    if(kmer != 0){

    }
    kmers.push_back(minSelfAndRevcomp(kmer));
        
        
        for(int i = 1; ; i++)
        {   

            kmer_start += jump_length;

            if( kmer_start + k  > l){
                break;
            }
            char c = s[i];
            std::cerr << c << std::endl;
            if( c == 'N' ){
                //kmers.push_back(0x8000000000000000ULL);
                skip ++;
                continue;
            }
            if(skip!=0){
               while(!convert(s + kmer_start, kmer)){
                kmer_start++;
               }
               skip = 0;
               kmers.push_back(minSelfAndRevcomp(kmer));
               continue;
            }
            kmer <<= 2;
            kNew = charToKey[*(s + kmer_start - 1 + k) - 'A'];
            kmer = (kmer|kNew)&mask;
            kmers.push_back(minSelfAndRevcomp(kmer));
            std::cerr << kmer_start << " " << kmer << std::endl;
            if(kmer==261993005056){
            std::cerr << kmer_start << " " << kmer << std::endl;
           }
        
        }*/
    
}

void KmerCreater::extractKmer(const char *s, const int l,bool rc)
{
    // Not implement jump in kmer extraction
    int jump_length = 1; 
    int kmer_start = 0;
    int skip = 0;
    keyT mask = bitmask();
    keyT kmer=-1;
    keyT kNew;
    bool b;
    while(kmer_start + k - 1 < l){
        if(kmer == -1){

            b = convert(s+kmer_start,kmer);
            if(b){                
                if(!rc)
                    kmers.push_back((kmer));
                else
                    kmers.push_back((kmer));
                
            }
        }else{
            char c = s[kmer_start - 1 + k];
            
            if (c == 'N'){
                kmer = -1;
            }else{
                kmer <<= 2;
                kNew = charToKey[*(s + kmer_start - 1 + k) - 'A'];
                kmer = (kmer|kNew)&mask;
                if(!rc)
                    kmers.push_back((kmer));
                else
                    kmers.push_back((kmer));
            }
        }
        kmer_start++;

    }
    
}


// void KmerCreater::extractKmer(const char *s, const int l, bool rc)
// {
//     // Not implement jump in kmer extraction
//     int jump_length = 1; 
//     int kmer_start = 0;
//     keyT mask = bitmask();
//     keyT kmer;
//     convert(s + kmer_start, kmer);
//     if(rc){
//         kmers.push_back(minSelfAndRevcomp(kmer));
//     }else{
//         kmers.push_back((kmer));
//     }
    

//     keyT kNew;
//     for(int i = 1; ; i++)
//     {
//         kmer_start += jump_length;

//         if( kmer_start + k  > l){
//             break;
//         }

//         kmer <<= 2;
//         kNew = charToKey[*(s + kmer_start - 1 + k) - 'A'];
//         kmer = (kmer|kNew)&mask;
//         if(rc){
//             kmers.push_back(minSelfAndRevcomp(kmer));
//         }else{
//             kmers.push_back((kmer));
//         }
    
    
//     }
// }
/*
void KmerCreater::extractKmer(const char *s, const int l,

    std::unordered_map<keyT,int> &km, valueT &v)

{
    // Not implement jump in kmer extraction
    int jump_length = 1; 
    int kmer_start = 0;
    keyT mask = bitmask();
    keyT kmer;
    keyT kNew;
    convert(s + kmer_start, kmer);
    keyT small = minSelfAndRevcomp(kmer);

    auto check = km.find(small);
    if(check != km.end()){

        check->second++;
        // if(check->second.size < 3){
        //     check->second.update(v);
        // }
        
    }else{
        // KmerStatus ks(v);
        km[small] = 1;

    }
    
    for(int i = 1; ; i++)
    {
        kmer_start += jump_length;

        if( kmer_start + k  > l){
            break;
        }

        kmer <<= 2;
        kNew = charToKey[*(s + kmer_start - 1 + k) - 'A'];
        kmer = (kmer|kNew)&mask;
        small = minSelfAndRevcomp(kmer);
        auto check = km.find(small);
        if(check != km.end()){

            check->second++;
            // if(check->second.size < 3){
            //     check->second.update(v);
            // }
            
        }else{
          //KmerStatus ks(v);
          km[small] = 1;

        }
    
    }
}*/
// void kToString(const keyT key){

// }
keyT KmerCreater::bitmask()
{
    switch(k)
    {   case 5:
            return 0x1F;
        case 10:
            return 0x000003ff;
        case 12:
            return 0x00000fff;
        case 16:
            return 0xFFFFFFFF;
        case 18:
            return 0xFFFFFFFFF;
        case 20:
            return 0xFFFFFFFFFF;
        case 21:
        	return 0x3FFFFFFFFFF;
        case 22:
            return 0xFFFFFFFFFFF;
        case 24:
            return 0xFFFFFFFFFFFF;
        case 25:
            return 0x3FFFFFFFFFFFF;
        case 28:
            return 0xFFFFFFFFFFFFFF;
        case 29:
            return 0x3FFFFFFFFFFFFFF;
        case 30:
            return 0xFFFFFFFFFFFFFFF;
        case 31:
            return 0x3FFFFFFFFFFFFFFF;                                                    
        default:
            std::cout << "error! invalid kmer_length: " << k << std::endl;
            exit(1); 
    }
}

void KmerCreater::convertString(char *s, const keyT &kin) {
    std::string str;
    keyT key = kin;

    for (int i = 0 ; i < k; i++) {
        str.push_back("ACGT"[key & 3]);
        key >>=2;
    }

    std::reverse(str.begin(),str.end());
    strcpy(s, str.c_str());

}

std::string KmerCreater::convertString(const keyT &kin) {
    std::string str;
    keyT key = kin;

    for (int i = 0 ; i < k; i++) {
        str.push_back("ACGT"[key & 3]);
        key >>=2;
    }

    std::reverse(str.begin(),str.end());
    return str;

}

keyT KmerCreater::convertKmer2Key(char *s, int k)
{
    int tmpLength = 0;
    switch (*s) 
    {
        case 'A':
        case 'C':
        case 'G':
        case 'T':
            keyT ret = 0;
            while (*s == 'A' || *s == 'C' || *s =='T' || *s =='G')
            {
                ret <<=2;
                switch (*s) 
                {
                    case 'T':
                        ret ++;
                        // jp cout <<1<<endl;
                    case 'G':
                        ret ++;
                        // jp cout << 2<< endl;
                    case 'C':
                        // jp cout << 3 << endl;
                        ret ++;
                    
                    // jp std::bitset<8> x(ret);    
                    //cout << "ret" << ":" <<  x << endl;
                }

                // std::bitset<8> y(ret);
                // cout << *s << ":" << ret << ":" << y<< endl;
                s++;
                tmpLength ++;
                if(tmpLength == k)
                {
                    return ret;
                }
            }
            //cout << "error in convertKmer2Key" << endl;
            //exit(1);
    }
    //cout << "error in convertKmer2Key" << endl;
    //exit(1);        
}

void KmerCreater::clear(){
    kmers.clear();
}

void KmerCreater::updateKmers(int pos, keyT key){
    kmers[pos] = key;
}

