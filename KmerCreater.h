#pragma once

#include <bitset>
#include "common.h"
#include <iostream>
#include <algorithm>
#include <unordered_map>
#include <unordered_set>
#include <cstring>
//	#include "OthelloIndex.h"
//@to do and nextNc, mask, 
// typedef unsigned long long keyT;
// typedef uint64_t valueT;
const keyT charToKey[26] = {0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x2, //ABCDEFG
                        0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, //HIJKLMN
                        0x0, 0x0, 0x0, 0x0, 0x0, 0x3,      //OPQRST
                        0x0, 0x0, 0x0, 0x0, 0x0, 0x3};//HIJKLMN
struct KmerStatus{
	KmerStatus():genes(3){};
	KmerStatus(valueT &v);
	std::unordered_set<valueT> genes;
	int size;
	void update(valueT &v);
};


class KmerCreater {
public:
	KmerCreater(){};
	KmerCreater(int stride, int klen,int r_len) : stride(stride), 
		k(klen),r_len(r_len),kmers(r_len,0) {
			mask = bitmask();
		};

	~KmerCreater(){};
	void init(int s, int klen){
		stride = s;
		k = klen;
		//std::cerr << stride <<" " << k << std::endl;
		mask = bitmask();
	}
	// cut seq into kmers and return its min
	unsigned char reverseBitsInByte(unsigned char v);
	keyT reverseComplement(keyT key);
	keyT minSelfAndRevcomp(const keyT key);
	bool convert(const char *s, keyT &k, int len);
	bool convert(const char *s, keyT &k);
	keyT bitmask();
	void extractKmer(const char *s, const int l);
	void extractKmer(const char *s, const int l, bool rc);
	void extractKmer(const char *s, const int l,
    	std::unordered_map<keyT, int> &km, valueT &v);
	std::string convertString(const keyT &kin);
	void convertString(char *s, const keyT &kin);
	keyT convertKmer2Key(char *s, int klen);
	void updateKmers(int pos, keyT key);
	keyT NextKey(keyT &lkey, const char* s, int& pos);
	keyT NextKey(keyT &lkey, const char* s, int& pos,int move);

	void clear();
	keyT nextNc;
	keyT mask;
	int stride;
	int k;
	int r_len;
	std::vector<keyT> kmers;
};