appname := FastCount

CXX := g++
CXXFLAGS := -w -g -std=c++11 -I othlib -march=native -pthread -fopenmp

LDFLAGS = -lz

srcfiles := $(shell find . -maxdepth 2 -name "*.cc")
objects  := $(patsubst %.cc, %.o, $(srcfiles))

all: $(appname)

$(appname): $(objects)
	$(CXX) $(CXXFLAGS) -o $(appname) $(objects) $(LDLIBS) $(LDFLAGS)

depend: .depend

.depend: $(srcfiles)
	rm -f ./.depend
	$(CXX) $(CXXFLAGS) -MM $^>>./.depend;

clean:
	rm -f $(objects)

dist-clean: clean
	rm -f *~ .depend

include .depend
