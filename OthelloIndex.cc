#include "OthelloIndex.h"
#include "KmerCreater.h"
#include "infix_iterator.h"
#include <fstream>

#ifndef KSEQ_INIT_READY
#define KSEQ_INIT_READY
KSEQ_INIT(gzFile, gzread)
#endif

using KVP = KV6432;

/* Build double index */
void OthelloIndex::BarcodeIndex(uint8_t size, Opts& opt){
  std::ifstream finput(opt.barcode);
  std::string str;
  KmerCreater kc(1, k, 0);
  if(! finput.is_open()){
    std::cerr << "Error: could not open barcode library file" << std:: endl;
    exit(1);
  }
  while(getline(finput,str)){
    // std::vector <std::string> col = splitStr(str,'-');
    // string s1 = col[0];
    string s1 = str;
    keyT key;
    kc.convert(s1.c_str(), key, 16);
    mapbc[key] = 1;
  }
  std::cerr << "load " << mapbc.size() << " barcodes" << std::endl;

  // vector<keyT> kV;
  // vector<valueT> vV; 
  // ConstantLengthKmerHelper<keyT, valueT> iohelper(size,0);
  // KVP pp;
  // FileReader<keyT, valueT> *freader;
  // freader = new KmerFileReader< keyT,valueT > (opt.barcode.c_str(), &iohelper,false);
  
  // while (freader->getNext(&pp.k, &pp.v)) {
  //   //std::cerr << pp.k << " " << pp.v << std::endl;
  //   kV.push_back(pp.k);
  //   vV.push_back(pp.v);
  //   mapbc[pp.k] = pp.v;
  // }
  
  // int LLfreq = 24;
  // barcodeA = new Othello<uint64_t>(LLfreq,kV,vV,true,0);
  
  // srand(time(NULL));
  // barcodeB = new Othello<uint64_t>(LLfreq,kV,vV,true,0);
  
}


int OthelloIndex::ReadGeneId(std::string filename,bool is_map) {
  std::ifstream finput(filename);
  if(! finput.is_open()){
    std::cerr << "Error: could not open gene id file" << std:: endl;
    exit(1);
  }
  int i = 0;
  std::string str; 
  if(is_map){
    std::cerr << "Loading Gene IDs from a map file" << std::endl;
    std::map <int,std::string> temp;
    while(getline(finput,str)){
      std::vector <std::string> col = splitStr(str,'\t');
      temp[std::atoi(col[1].c_str())] = col[0];
      i++;
    }
    for(int j = 0; j < i;j++){
      geneids.emplace_back(temp[j+1]);
      //std::cout << j << " " << temp[j] << std::endl;
    }
    
  }else{
    std::cerr << "Loading Gene IDs from gene list" << std::endl;
    while(getline(finput,str)){
      geneids.emplace_back(str);
      i++;
    }
  }
  finput.close();
  finput.clear();
  return(i);
}


void OthelloIndex::LoadIndex(Opts& opt){

  // load othello index
  FILE* infile;
  unsigned char buf[0x20];
  infile = fopen(opt.index_file.c_str(),"rb");
  std::cerr << "1. loading index ..." << std::endl;
  if(infile){
    std::cerr << "	Loading Othello index" << std::endl;
  }else{
    std::cerr << "Error: could not open index" << opt.index_file << std::endl;
    exit(1);
  }
  fread(buf,1,sizeof(buf),infile);
  othello = new Othello<keyT> (buf);
  othello->loadDataFromBinaryFile(infile);
  fclose(infile);
  std::cerr << "	Loading Othello index done" << std::endl;
  
  // load geneids
  int tot_genes = ReadGeneId(opt.geneids_file.c_str(),true);

  std::cerr << "	" << tot_genes << " Genes in the index file!" << std::endl;
  num_genes = geneids.size();
  if(!opt.sgRNA){
    std::ifstream infs;
    // load gene cliques mapping
    infs.open(opt.cliques_file);
    if(infs.is_open()){
      std::cerr << "	Loading Gene Cliques" << std::endl;
      std::string str;
      std::vector<std::string> col1(2);
      std::vector<std::string> col2(3);

      while(getline(infs,str)){
        col1 = splitStr(str,'\t');
        col2 = splitStr(col1[1],',');
        if (!col2.empty() && col2.size() > 1){
          GeneClique gc(col2);
          cliqueidx.insert({std::atoi(col1[0].c_str()),gc});
        }

      }
      cliqs_begin = num_genes + 1;
      if(cliqueidx.size()==0){
      	cliqs_end = cliqs_begin;
      }else{
      	cliqs_end = cliqs_begin + cliqueidx.size() - 1;
      }
      
            infs.close();
    }else{
      std::cerr << "Error: Loading Gene Cliques FAILED!" << std::endl;
      exit (EXIT_FAILURE);
    }
    std::cerr << "	Loading Gene Cliques done" << std::endl;
  }else{
    cliqs_begin = num_genes + 1;
    cliqs_end = cliqs_begin;
  }

  if(doubleCheck){
    infile = fopen(opt.index2_file.c_str(),"rb");
    if(infile){
      std::cerr << "	Loading Othello index guided for sgRNA" << std::endl;
    }else{
      std::cerr << "Error: could not open index " << opt.index2_file << std::endl;
      exit(1);
    }
    fread(buf,1,sizeof(buf),infile);
    othelloG = new Othello<keyT> (buf);
    othelloG->loadDataFromBinaryFile(infile);
    fclose(infile);
  	std::cerr << "	Loading Othello index guided for sgRNA done" << std::endl;
  }
}


/* Build index for sgRNA */
void OthelloIndex::BuildSgRNA(Opts& opt){
  std::ifstream finput(opt.transfasta[0]);

  if(! finput.is_open()){
    std::cerr << "Error: could not open sgRNA library file" << std:: endl;
    exit(1);
  }

  std::string str;
  int i=0;
  std::ofstream gidxO;
  std::ofstream kmerfile;
  gidxO.open(opt.geneids_file.c_str());
  kmerfile.open(opt.sgrna_file.c_str());
  std::set<std::string> lib_count;
  std::set<std::string>::iterator it;
  //std::map<std::string, std::vector<std::string>> eq_sgrna;
  //KmerCreater kc(1, k, 0);
  //char kstring[k+1];
  int removed = 0;
  /* parse target sequences, remove dup */
  while(getline(finput,str)){
  //skip the header
    //std::cerr << str << std::endl;
    if (i==0){
      i++;
      continue;
    }
    
    std::vector <std::string> col = splitStr(str,'\t');
    //kc.clear();
    //kc.extractKmer(col[2].c_str(),k,false);
    //std::cerr<<kc.kmers[0] << std::endl;
    
    //kc.convertString(kstring,kc.kmers[0]);
    //std::cerr<<kstring << std::endl;
    //library file format
    //gene_id,UID,seq
    it = lib_count.find(col[2]);
    if(it == lib_count.end()){

        kmerfile << std::string(col[2]) << "\t" << i << "\n";
        gidxO << col[1] << "\t" << i << "\n";
        i++;
        lib_count.insert(std::string(col[2]));

     }else{
      	removed++;
     }
  }
  std::cerr << "Removed " << removed << " sgRAN from the library!" << std::endl;
  /* parse transRNA seq */
  /*
  gzFile fp = 0;
  kseq_t *seq;
  vector<keyT> kV;
  vector<valueT> vV;  
  int l = 0;
  fp = gzopen("transRNA.fa", "r");
  seq = kseq_init(fp);
  while (true) {
    valueT fix = 888888;
    l = kseq_read(seq);
    if (l <= 0) {
      break;
    }
    kc.clear();
    kc.extractKmer(seq->seq.s,seq->seq.l,false);
    for(auto k:kc.kmers){
      kc.convertString(kstring,k);
      //std::cerr << k << " " << kstring << std::endl;
      kV.push_back(k);
      vV.push_back(fix);
    }
    std::cerr << "[build] add transRNA sequences...\n";
  }
  kmerfile.close();
  gidxO.close();
  */
  vector<keyT> kV;
  vector<valueT> vV; 

  ConstantLengthKmerHelper<keyT, valueT> iohelper(k,0);
  KVP pp;
  FileReader<keyT, valueT> *freader;
  freader = new KmerFileReader< keyT,valueT > (opt.sgrna_file.c_str(), &iohelper,false);

  while (freader->getNext(&pp.k, &pp.v)) {
    //std::cerr << pp.k << " " << pp.v << std::endl;
    kV.push_back(pp.k);
    vV.push_back(pp.v);
  }

  Othello<keyT> *othe;
  int LLfreq = 32;
  std::cerr << "Create othello...\n";
  std::cerr << kV.size() << " " << vV.size() << std::endl;
  othe = new Othello<uint64_t>(LLfreq,kV,vV,true,0);
  FILE *pfile;
  std::cerr << "Write othello...\n";
  pfile = fopen(opt.index_file.c_str(),"wb");
  unsigned char buf[0x20];
  othe->exportInfo(buf);
  fwrite(buf,sizeof(buf),1,pfile);
  othe->writeDataToBinaryFile(pfile);
  fclose(pfile);

  srand(time(NULL));
  std::cerr << "Create othello guides...\n";
  Othello<keyT> *otheS;
  LLfreq= 32;
  otheS = new Othello<uint64_t>(LLfreq,kV,vV,true,0);
  pfile = fopen(opt.index2_file.c_str(),"wb");
  otheS->exportInfo(buf);
  fwrite(buf,sizeof(buf),1,pfile);
  otheS->writeDataToBinaryFile(pfile);
  fclose(pfile);
  
}

/* Build index for mRNA seq */
void OthelloIndex::BuildIndex(Opts& opt){
  
  ConstantLengthKmerHelper<keyT, valueT> iohelper(k,0);
  KVP pp;
  FileReader<keyT, valueT> *freader;
  freader = new KmerFileReader< keyT,valueT > (opt.kmer_file.c_str(), &iohelper, false);
  vector<keyT> kV;
  vector<valueT> vV;
  while (freader->getNext(&pp.k, &pp.v)) {
    kV.push_back(pp.k);
    vV.push_back(pp.v);
    //std::cerr << pp.k << " "  << pp.v << std::endl;
  }
  freader = new KmerFileReader< keyT,valueT > (opt.info_kmer_file.c_str(), &iohelper,false);
  while (freader->getNext(&pp.k, &pp.v)) {
    kV.push_back(pp.k);
    vV.push_back(pp.v);
  }
  freader = new KmerFileReader< keyT,valueT > (opt.rep_kmer_file.c_str(), &iohelper,false);
  while (freader->getNext(&pp.k, &pp.v)) {
    kV.push_back(pp.k);
    vV.push_back(pp.v);
  }
  Othello<keyT> *othe;
  int LLfreq = 24;
  std::cerr << "Create othello...\n";
  std::cerr << kV.size() << " " << vV.size() << std::endl;
  othe = new Othello<uint64_t>(LLfreq,kV,vV,true,0);

  FILE *pfile;
  std::cerr << "Write othello...\n";
  pfile = fopen(opt.index_file.c_str(),"wb");
  unsigned char buf[0x20];
  othe->exportInfo(buf);
  fwrite(buf,sizeof(buf),1,pfile);
  othe->writeDataToBinaryFile(pfile);
  fclose(pfile);

}


/* Kmer Counting for mRNA seq */
void OthelloIndex::CountKmer(Opts &opt){
  int k = opt.k;
  int countGenes = 0;
  valueT grec = 1;
  // store gene
  std::unordered_set<std::string> gene_names;
  std::unordered_map<std::string, std::vector<valueT>> infoKmers;
  // all seqs
  std::vector<std::string> seqs;
  // seq index for gene
  std::unordered_map<std::string, std::vector<int>> gene_seqs;
  std::map<std::string, int> used;
  // list of transcripts to use
  std::ifstream strandin(opt.txlist);
  std::string tmp;
  while(getline(strandin,tmp)){
    //std::cerr << tmp << std::endl;
    used[tmp] = 1;
  }
  // strands["ENSG00000167995"] = true;
  // strands["ENSG00000167996"] = false;




  if (opt.parse_gencode){
    std::cerr << "Infers gene name using the fasta sequence id ..." << std::endl;
  }else{
    if (opt.tx2gene_file){
      std::cerr <<"Uses transcript to gene mapping in provided file..." << std::endl;
      //ADD parsetrname()
    }
  }
  // read fasta file  
  gzFile fp = 0;
  kseq_t *seq;
  int l = 0;
  int countSeq = 0;
  int countTx = 0;


  for (auto& fasta : opt.transfasta) {
    std::cerr << "Process " << fasta << std::endl;
    fp = gzopen(fasta.c_str(), "r");
  	if (!fp) {
    	std::cerr << "Error: could not open file " << fasta.c_str() << std::endl;    
    	throw std::exception();
  	}
    seq = kseq_init(fp);
    while (true) {
      l = kseq_read(seq);
      if (l <= 0) {
        break;
      }
      countSeq++;
      seqs.emplace_back(seq->seq.s);
      target_lens_.emplace_back(seq->seq.l);
      /*
      
      auto n = str.size();
      for (auto i = 0; i < n; i++) {
        char c = str[i];
        c = ::toupper(c);
        if (c=='U') {
          str[i] = 'T';
          countUNuc++;
        } else if (c !='A' && c != 'C' && c != 'G' && c != 'T') {
          //str[i] = Dna(gen()); // replace with pseudorandom string
        }
      }
     */
      std::string& str = *seqs.rbegin();
      std::transform(str.begin(), str.end(),str.begin(), ::toupper);

      /*
      if (str.size() >= 10 && str.substr(str.size()-10,10) == "AAAAAAAAAA") {
        // clip off polyA tail
        int j;
        for (j = str.size()-1; j >= 0 && str[j] == 'A'; j--) {}
        str = str.substr(0,j+1);
      }
      */

    
      

      std::string name(seq->name.s);
      std::string gene;
      std::string transcript;
      // parse gencode: txid|gid|....|
      if(opt.parse_gencode){
        std::vector<std::string> item = splitStr(name, '|');
        gene = item[1];
        transcript = splitStr(item[0], '.')[0];
        //std::cerr << transcript << std::endl;
        auto itt = used.find(transcript);
        // if the transcript is not in the transcripts list, skip it.
        // if(itt == used.end())
        //   //std::cerr << "skip " << transcript << endl;
        //   continue;

      }
      countTx++;
      //std::cerr << "found" << std::endl;

      // add gene name for each sequence to gene_names
      gene_names.insert(gene);
      // update gene seq list
      auto g = gene_seqs.find(gene);
      if (g != gene_seqs.end()) {
        g->second.push_back(countSeq-1);
        //std::cerr << "push" << std::endl;
      }else{
        //std::cerr << "add" << std::endl;
        //gene_seqs.insert(std::make_pair(gene,vector<int>(countSeq-1)));
        gene_seqs[gene] = vector<int>();
        gene_seqs[gene].push_back(countSeq-1);
      }


    }
    std::cerr << "total tx: " << countSeq << std::endl;
    std::cerr << "used tx: " << countTx << std::endl;
    gzclose(fp);
    fp=0;
  }
  //kmer_count: track kmer appearance among genes
  std::unordered_map<keyT, int> kmer_count;
  KmerCreater kc(1, k, 0);

  for(auto &g:gene_seqs){
    std::set<keyT> keys;
    keys.clear();
    // Extract kmers for each gene
    
    for(auto &idx:g.second){
      kc.clear();
      kc.extractKmer(seqs[idx].c_str(),target_lens_[idx],true);
      std::sort(kc.kmers.begin(),kc.kmers.end());
      std::set<keyT> ktmp(kc.kmers.begin(),kc.kmers.end());
      keys.insert(ktmp.begin(),ktmp.end());
    }
    // Add kmer count
    for(auto &kv : keys){
      //std::cerr << kv << std::endl;
      auto search = kmer_count.find(kv);
      if(search!=kmer_count.end()){
        search->second++;
      }else{
        kmer_count[kv] = 1;
      }
    }
    grec++;
  }
  /*for(auto &km : kmer_count){
    char * kstring;
    kc.convertString(kstring, km.first);
    std::cout << kstring << "\t"; 
    std:: cout << km.second<< std::endl;
  }*/
  GeneIndex(gene_names);
  std::ofstream gidxO;
  gidxO.open(opt.geneids_file);
  for(auto &g:gene_to_index){
    gidxO << g.first << "\t" << g.second << "\n";
  }
  gidxO.close();

  std::cerr << "Extracted kmers... " << std::endl;
  std::ofstream repkmerO;
  std::ofstream infokmerO;
  std::ofstream uniqkmerO;
  std::ofstream pseudoGO;
  repkmerO.open(opt.rep_kmer_file.c_str());
  infokmerO.open(opt.info_kmer_file.c_str());
  pseudoGO.open(opt.cliques_file.c_str());
  uniqkmerO.open(opt.kmer_file.c_str());
  char kstring[k+1];

  for(auto &g:gene_seqs){
    std::set<keyT> keys;
    keys.clear();
    // Extract kmers for each gene
    for(auto &idx:g.second){
      kc.clear();
      kc.extractKmer(seqs[idx].c_str(),target_lens_[idx],true);
      std::sort(kc.kmers.begin(),kc.kmers.end());
      std::set<keyT> ktmp(kc.kmers.begin(),kc.kmers.end());
      keys.insert(ktmp.begin(),ktmp.end());
      
    }
    // Add kmer count
    valueT gidx = gene_to_index[g.first];
    for(auto &kv : keys){
      auto search = kmer_count.find(kv);
      kc.convertString(kstring, search->first);
      string tmp(kstring);
      //std::cerr << "gene id: " << g.first << "key: " << search->first << endl;
      if (search->second == -1){
        continue;
      }
      //is unique kmer
      if( search->second == 1 ){
        uniqkmerO << kstring << "\t" << gidx << "\n";
      }else if(search->second <= 50){
        auto f = infoKmers.find(kstring);
        if(f != infoKmers.end()){
          f->second.push_back(gidx);
        }else{
          infoKmers[kstring] = vector<valueT> ();
          infoKmers[kstring].push_back(gidx);
        }
      }else{ //is repetitive kmer, always has 0 id
        kmer_count[kv] = -1;
        repkmerO << kstring << "\t" << 0 << "\n";
      }
    }

    //gidx++;
  }
   //infoKmers are now pseudo uniq kmers
  std::map<string,uint32_t> grp_map;
  int grp_start = gene_to_index.size();

  for(auto &ik:infoKmers){
    std::ostringstream out;
    std::sort(ik.second.begin(),ik.second.end());
    std::copy(ik.second.begin(), ik.second.end(), infix_ostream_iterator<valueT>(out, ","));
    if (grp_map.find(out.str()) == grp_map.end()){
    	grp_start++;
     	grp_map.insert(make_pair(out.str(),grp_start));
    }
    infokmerO << ik.first << "\t" << grp_map[out.str()] <<  "\n";

  }
  if(infoKmers.size()!=0) {
  	 fprintf(stderr,"Info kmer start = %d, end = %d",gene_to_index.size(),grp_start); 
  }
  for (auto &g : grp_map){
    pseudoGO << g.second  << "\t" << g.first<< "\n";
  }


  std::cerr <<"Output info kmers Done! index ends at " << grp_start << std::endl;
  uniqkmerO.close();
  infokmerO.close();
  repkmerO.close();
  pseudoGO.close();
  std::cerr <<"Build Othello index... " << std::endl;
  
}

  /*
  int num_trans = seqs.size();
  int num_genes = gene_names.size();
  
  //GeneIndex(gene_names);
  std::ofstream gidxO;
  gidxO.open("gene_index.txt");
  for(auto &g:gene_to_index){
    gidxO << g.first << "\t" << g.second << "\n";
  }


  

  KmerCreater kc(1, k, 0);
  for(int i = 0; i < num_trans; i++){
    
    kc.clear();
    kc.extractKmer(seqs[i].c_str(),target_lens_[i],kmer_map,
      tx_ids[i]);
    
  }
  std::cerr << "[build] extracted kmers... " << std::endl;
  std::ofstream repkmerO;
  std::ofstream infokmerO;
  std::ofstream uniqkmerO;
  std::ofstream pseudoGO;
  repkmerO.open("rep.txt");
  infokmerO.open("info.txt");
  pseudoGO.open("pseudogene.txt");
  uniqkmerO.open("uniq.txt");
  char km[22];
  for(int i = 0; i < num_trans; i++){  
    kc.clear();
    kc.extractKmer(seqs[i].c_str(),target_lens_[i]);
    for(auto &k:kc.kmers){
      auto check = kmer_map.find(k);
      kc.convertString(km,k);
      // is uniq kmer
      if(check->second==1){
        uniqkmerO << km << "\t" << tx_ids[i] << "\n";
      }else if(check->second > 3){
        // is repetitive kmer
        repkmerO << km << "\t" << 0 << "\n";
      }else{
        // is info kmer
        auto f = infoKmers.find(km);
        if(f != infoKmers.end()){
          f->second.insert(tx_ids[i]);
        }else{
          std::set<valueT> tmp;
          tmp.insert(tx_ids[i]);
          infoKmers[km] = tmp;
        }
      }

    }
  }

  //infoKmers are now pseudo uniq kmers
  std::cerr <<"[build] output info kmers, index starts at " << gidx << std::endl;
  for(auto &ik:infoKmers){
    infokmerO << ik.first << "\t" << gidx << "\n";
    std::ostringstream out;
    std::copy(ik.second.begin(), ik.second.end(), infix_ostream_iterator<valueT>(out, ","));
    pseudoGO << gidx << "\t" << out << "\n";
    gidx++;
  }
  std::cerr <<"[build] output info kmers Done! index ends at " << gidx-1 << std::endl;

  // //KmerCreater kc(1, k, 0);
  // for(auto &kmer : kmer_map){
  //   char km[22];
  //   kc.convertString(km,kmer.first);
  //   //std::cout << km << std::endl;
  //   if(kmer.second.size > 3){
  //     repkmerO << km << "\t" << 0 << "\n";
  //   }else if(kmer.second.size != 1){
  //     infokmerO << km << "\t" << gidx << "\n";
  //     gidx++;
  //   }else{
  //     auto f = kmer.second.genes.begin();
  //     uniqkmerO << km << "\t" << *f << "\n";
  //   }
  // }

  // std::vector<keyT> kV;
  // std::vector<valueT> vV;



}
*/

void OthelloIndex::GeneIndex(std::unordered_set<std::string> &gids){
  int i = 1;
  for(auto &g:gids){
    gene_to_index[g] = i;
    i++;
  }
}

GeneClique OthelloIndex::GetGeneClique(valueT val) const{
  auto clique = cliqueidx.find(val);

  return clique->second;
}

// GeneClique::GeneClique(std::vector<std::string>& genes){
//   if (genes.size() < 2 || genes.size() > 3){
//     std::cerr << "Error: should have less than 4 genes in the clique ..." << std::endl;
//     exit(1);
//   }
//   size = genes.size();
//   g1 = std::atoi(genes[0].c_str());
//   gene_vec.push_back(g1);
//   g2 = std::atoi(genes[1].c_str());
//   gene_vec.push_back(g2);
//   if (genes.size() == 3){
//     g3 = std::atoi(genes[2].c_str());
//     gene_vec.push_back(g3);
//   }
// }


GeneClique::GeneClique(std::vector<std::string>& genes){
  // if (genes.size() < 2 || genes.size() > 3){
  //   std::cerr << "Error: should have less than 4 genes in the clique ..." << std::endl;
  //   exit(1);
  // }
  size = genes.size();
  for(auto & i:genes){
    gene_vec.push_back(std::atoi(i.c_str()));
  }
  // g1 = std::atoi(genes[0].c_str());
  // gene_vec.push_back(g1);
  // g2 = std::atoi(genes[1].c_str());
  // gene_vec.push_back(g2);
  // if (genes.size() == 3){
  //   g3 = std::atoi(genes[2].c_str());
  //   gene_vec.push_back(g3);
  // }
}
// GeneClique GeneClique::fill(vector<int>& genes){
//  GeneClique tmp = new GeneClique;
//  if (genes.size() < 2){
//    std::cerr << "Error: less than 2 genes in the clique" << std::endl;
//    exit(1);
//  }
//  tmp.g1 = std::atoi(genes[0]);
//  tmp.g2 = std::atoi(genes[1]);
//  if (genes.size() == 3){
//    tmp.g3 = std::atoi(genes[2]);
//  }

//  return GeneClique;
// }

/* NOT IN USE*/
void OthelloIndex::match(const char *s, int l,
  std::vector<std::pair<valueT,int>> &v) const{
  valueT val;
  KmerCreater kc(stride,k,0);
  kc.extractKmer(s,l);
  // find the first valid key
  // valid key: 0 - cliqs_end
  int i = 0;
  int last_p = kc.kmers.size() - 1;
  while (i < last_p){
    
    val = othello->queryInt(kc.kmers[i]);
    v.push_back({val,i});
    i++;
    // if ( val > cliqs_end){
    //   v.push_back({val,i});
    //   i++;
    // }else{
    //   v.push_back({val,i});
    //   i++;
    //   break;
    // }
  }

  // for( i ; i < last_p; i++){
  //   // jump not implemented yet
  //   char str[22];
  //   keyT km = kc.kmers[i];
  //   kc.convertString(str,&km);
  //   // std:: cout << kc.kmers[i] << ":" << str << std::endl;
  //   val = othello->queryInt(kc.kmers[i]);
  //   // valid key
  //   if ( val <= cliqs_end){
  //     v.push_back({val,i});
  //   }else{
  //     v.push_back({val,i});
  //   }
  // }

}



/*
void OthelloIndex::BuildIndex(Opts &opt){
  int k = opt.k;
  int countGenes = 0;
  valueT gidx = 1;
  // store gene
  std::unordered_set<std::string> gene_names;
  std::unordered_set<std::string> tx_names;
  std::vector<valueT> tx_ids;
  std::unordered_map<std::string,std::set<valueT>> infoKmers;
  std::vector<std::string> seqs;
  if ( opt.parse_gencode ){
    std::cerr << "[index] infers gene name using the fasta sequence id ...";
    std::cerr << std::endl;
  }else{
    if ( opt.tx2gene_f ){
      std::cerr <<"[index] uses transcript to gene mapping in provided file...";
      std::cerr << std::endl;
      //ADD parsetrname()
    }
  }
  // read fasta file  
  gzFile fp = 0;
  kseq_t *seq;
  int l = 0;
  int countNonNucl = 0;
  int countUNuc = 0;
  int polyAcount = 0;
  for (auto& fasta : opt.transfasta) {
    std::cerr << fasta << std::endl;
    fp = gzopen(fasta.c_str(), "r");
    seq = kseq_init(fp);
    while (true) {
      l = kseq_read(seq);
      if (l <= 0) {
        break;
      }
      seqs.emplace_back(seq->seq.s);
      std::string& str = *seqs.rbegin();
      auto n = str.size();
      for (auto i = 0; i < n; i++) {
        char c = str[i];
        c = ::toupper(c);
        if (c=='U') {
          str[i] = 'T';
          countUNuc++;
        } else if (c !='A' && c != 'C' && c != 'G' && c != 'T') {
          //str[i] = Dna(gen()); // replace with pseudorandom string
          countNonNucl++;
        }
      }
      std::transform(str.begin(), str.end(),str.begin(), ::toupper);

      if (str.size() >= 10 && str.substr(str.size()-10,10) == "AAAAAAAAAA") {
        // clip off polyA tail
        //std::cerr << "[index] clipping off polyA tail" << std::endl;
        polyAcount++;
        int j;
        for (j = str.size()-1; j >= 0 && str[j] == 'A'; j--) {}
        str = str.substr(0,j+1);
      }

    
      target_lens_.push_back(seq->seq.l);
      std::string name(seq->name.s);
      // parse gencode: txid|gid|....|
      if(opt.parse_gencode){
        std::vector<std::string> item = splitStr(name, '|');
        std::string gene = item[1];
        std::string tx = item[0];
        name = tx;
        auto id = gene_to_index.find(gene);
        if ( id != gene_to_index.end()) {
          tx_ids.push_back(id->second);
        }else{
          gene_to_index[gene] = gidx;
          tx_ids.push_back(gidx);
          gidx++;

        }
        
      }
      // size_t p = name.find(' ');
      // if (p != std::string::npos) {
      //   name = name.substr(0,p);
      // }
      // if (tx_names.find(name) != tx_names.end()) {
      //   if (!opt.make_unique) {
      //     std::cerr << "Error: repeated name in FASTA file " << fasta << "\n" << name << "\n\n" << "Run with --make-unique to replace repeated names with unique names" << std::endl;
      //     exit(1);
      //   } else {
      //     for (int i = 1; ; i++) { // potential bug if you have more than 2^32 repeated names
      //       std::string new_name = name + "_" + std::to_string(i);
      //       if (tx_names.find(new_name) == tx_names.end()) {
      //         name = new_name;
      //         break;
      //       }
      //     }
      //   }
      // }
      // tx_names.insert(name);
      

    }
    gzclose(fp);
    fp=0;
  } 
  if (polyAcount > 0) {
    std::cerr << "[build] warning: clipped off poly-A tail (longer than 10)" << std::endl << "        from " << polyAcount << " target sequences" << std::endl;
  }

  
  if (countNonNucl > 0) {
    std::cerr << "[build] warning: replaced " << countNonNucl << " non-ACGUT characters in the input sequence" << std::endl << "        with pseudorandom nucleotides" << std::endl;
  }
  if (countUNuc > 0) {
    std::cerr << "[build] warning: replaced " << countUNuc << " U characters with Ts" << std::endl;
  }
  
  int num_trans = seqs.size();
  int num_genes = gene_names.size();
  
  //GeneIndex(gene_names);
  std::ofstream gidxO;
  gidxO.open("gene_index.txt");
  for(auto &g:gene_to_index){
    gidxO << g.first << "\t" << g.second << "\n";
  }


  std::unordered_map<keyT, int > kmer_map;

  KmerCreater kc(1, k, 0);
  for(int i = 0; i < num_trans; i++){
    
    kc.clear();
    kc.extractKmer(seqs[i].c_str(),target_lens_[i],kmer_map,
      tx_ids[i]);
    
  }
  std::cerr << "[build] extracted kmers... " << std::endl;
  std::ofstream repkmerO;
  std::ofstream infokmerO;
  std::ofstream uniqkmerO;
  std::ofstream pseudoGO;
  repkmerO.open("rep.txt");
  infokmerO.open("info.txt");
  pseudoGO.open("pseudogene.txt");
  uniqkmerO.open("uniq.txt");
  char km[22];
  for(int i = 0; i < num_trans; i++){  
    kc.clear();
    kc.extractKmer(seqs[i].c_str(),target_lens_[i]);
    for(auto &k:kc.kmers){
      auto check = kmer_map.find(k);
      kc.convertString(km,k);
      // is uniq kmer
      if(check->second==1){
        uniqkmerO << km << "\t" << tx_ids[i] << "\n";
      }else if(check->second > 3){
        // is repetitive kmer
        repkmerO << km << "\t" << 0 << "\n";
      }else{
        // is info kmer
        auto f = infoKmers.find(km);
        if(f != infoKmers.end()){
          f->second.insert(tx_ids[i]);
        }else{
          std::set<valueT> tmp;
          tmp.insert(tx_ids[i]);
          infoKmers[km] = tmp;
        }
      }

    }
  }

  //infoKmers are now pseudo uniq kmers
  std::cerr <<"[build] output info kmers, index starts at " << gidx << std::endl;
  for(auto &ik:infoKmers){
    infokmerO << ik.first << "\t" << gidx << "\n";
    std::ostringstream out;
    std::copy(ik.second.begin(), ik.second.end(), infix_ostream_iterator<valueT>(out, ","));
    pseudoGO << gidx << "\t" << out << "\n";
    gidx++;
  }
  std::cerr <<"[build] output info kmers Done! index ends at " << gidx-1 << std::endl;

  // //KmerCreater kc(1, k, 0);
  // for(auto &kmer : kmer_map){
  //   char km[22];
  //   kc.convertString(km,kmer.first);
  //   //std::cout << km << std::endl;
  //   if(kmer.second.size > 3){
  //     repkmerO << km << "\t" << 0 << "\n";
  //   }else if(kmer.second.size != 1){
  //     infokmerO << km << "\t" << gidx << "\n";
  //     gidx++;
  //   }else{
  //     auto f = kmer.second.genes.begin();
  //     uniqkmerO << km << "\t" << *f << "\n";
  //   }
  // }

  // std::vector<keyT> kV;
  // std::vector<valueT> vV;



}

*/ 

/* Test build index */
void OthelloIndex::test(){
  int kmerlength = 21;
  ConstantLengthKmerHelper<uint64_t, uint32_t> iohelper(kmerlength,0);

    vector<KVP> VKmer;
    KVP pp;
    FileReader<uint64_t, uint32_t> *freader;
    freader = new KmerFileReader< uint64_t,uint32_t > ("info.txt", &iohelper,false);
  vector<uint64_t> kV;
  vector<uint32_t> vV;
    while (freader->getNext(&pp.k, &pp.v)) {
    kV.push_back(pp.k);
    vV.push_back(pp.v);
        VKmer.push_back(pp);
    //cout << pp.k << "-" << pp.v << endl;
    }
    // printf("Sorting %lu keys\n", VKmer.size());
  Othello<uint64_t> *othe;
  int LLfreq = 24;
  othe = new Othello<uint64_t>(LLfreq,kV,vV,true,0); 
}
