#pragma once
#include "othello.h"
#include "kseq.h"
#include <sstream>
#include "KmerCreater.h"
#include "common.h"
#include <unordered_map>
#include <unordered_set>
#include <cstdio>

#include <algorithm>
#include <cmath>
//#include <othellotypes.hpp> 

//#include "oltnew.h"

// max genes in the clique = 3
struct GeneClique{
	//fill(vector<int>& genes){};
	// note: this is a declaration
	GeneClique(std::vector<std::string>& genes);
	// note: GeneClique(std::vector<std::string>& genes) {};
	// note: is a definition of an empty body
	// note: what it does is just for initialization


	int g1;
	int g2; 
	int g3;
	int size;
	std::vector<valueT> gene_vec;
};




struct OthelloIndex{
	// contructor
	OthelloIndex(Opts& opt): k(opt.k), delim('\t'),stride(opt.stride),doubleCheck(opt.sgRNA){};
	
	//https://stackoverflow.com/questions/3141087/what-is-meant-with-const-at-end-of-function-declaration
	// The constant function: class variables are not editable.
	// v: the kmer assignment and its index on read
	void match(const char *s, int l, std::vector<std::pair<valueT,int>> &v) const;
	int k;
	int num_genes; // total number of genes
	int cliqs_begin; // the first index for cliques
	int cliqs_end; // the last index for cliques
	int i_repeats;
	char delim;
	int stride;
	bool doubleCheck = true;
	Othello<uint64_t> *barcodeA;
	Othello<uint64_t> *barcodeB;
	Othello<uint64_t> *othello;
	Othello<uint64_t> *othelloG;
	std::map<keyT, int> mapbc;

	std::vector<uint32_t> target_lens_;

	//map othello kVal to gene_symbols
	std::vector<std::string> geneids;
	std::unordered_map<std::string,valueT> gene_to_index;
	//map info kmers index to kVal
	std::unordered_map<uint32_t,GeneClique> cliqueidx;
	std::unordered_map<std::string, std::string> tx_to_gene;
	// function that load tx_to_gene mapping
	void LoadTxToGene(); // load transcript and gene mapping
	void ParseTxName(); // for gencode fasta file
	void CountKmer(Opts& opt);
	void BuildIndex(Opts& opt);
	void test();
	void GeneIndex(std::unordered_set<std::string> &gids);
	int ReadGeneId(std::string filename,bool is_map);
  	void BuildSgRNA(Opts& opt);
  	void LoadIndex(Opts& opt);
  	void BarcodeIndex( uint8_t size, Opts& opt);
	GeneClique GetGeneClique(valueT val)const;
};

// struct GeneAnnotation{
// 	std::string geneid;
// 	std::string 
// }

