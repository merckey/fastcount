#pragma once
#include "ProcessSeq.h"
#include <omp.h>
char prime3[] = "GTTTTAG\0";
char prime5[] = "AACACCG\0";
using namespace std::chrono;
/* Get hamming distance */
int hamming(char *a, char *b) {
  int h = 0;
  while (*a != 0 && *b != 0) {
    if (*a != *b) {
      h++;
    }
    a++;
    b++;
  }
  return h;
}
/* Get total distance to the guards for sgRNA*/
int ReadProcessor::distance(int pos, const char *s) {
  char t[10];
  strncpy(t, s + pos, 7);
  t[8] = '\0';
  int hdistA = hamming(prime3, t);
  strncpy(t, s + pos - k - 7, 7);
  t[8] = '\0';
  int hdistB = hamming(prime5, t);
//std::cerr << t << " " << hdistB << std::endl;
//std::cerr <<t <<  " hamming " << hdist << std::endl;
  int ttdist = hdistA + hdistB;
//free(t);
  return ttdist;
}


template <typename T>
void Append(std::vector<T>& a, const std::vector<T>& b)
{
  //a.reserve(a.size() + b.size());
  a.insert(a.end(), b.begin(), b.end());
}

// int HammingDistKeyT(keyT x, keyT y) {
//   keyT i = x ^ y;
//   i = (i & 0x5555555555555555) | ((i & 0xAAAAAAAAAAAAAAAA) >> 1);
//   i = i - ((i >> 1) & 0x5555555555555555);
//   i = (i & 0x3333333333333333) +
//       ((i >> 2) & 0x3333333333333333);
//   i = ((i + (i >> 4)) & 0x0F0F0F0F0F0F0F0F);
//   return (i * (0x0101010101010101)) >> 56;
// }


int popcount64d(uint64_t x)
{
  int count;
  for (count = 0; x; count++)
    x &= x - 1;
  return count;
}
char comp_tab[] = {
  0,   1, 2,   3,   4,   5, 6,   7,   8,   9,  10,  11,  12,  13,  14,  15,
  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  26,  27,  28,  29,  30,  31,
  32,  33,  34,  35,  36,  37,  38,  39,  40,  41,  42,  43,  44,  45,  46,  47,
  48,  49,  50,  51,  52,  53,  54,  55,  56,  57,  58,  59,  60,  61,  62,  63,
  64, 'T', 'V', 'G', 'H', 'E', 'F', 'C', 'D', 'I', 'J', 'M', 'L', 'K', 'N', 'O',
  'P', 'Q', 'Y', 'S', 'A', 'A', 'B', 'W', 'X', 'R', 'Z',  91,  92,  93,  94,  95,
  64, 't', 'v', 'g', 'h', 'e', 'f', 'c', 'd', 'i', 'j', 'm', 'l', 'k', 'n', 'o',
  'p', 'q', 'y', 's', 'a', 'a', 'b', 'w', 'x', 'r', 'z', 123, 124, 125, 126, 127
};

std::vector<valueT> intersect(const std::vector<valueT>& x, const std::vector<valueT>& y) {
  std::vector<valueT> v;
  auto a = x.begin();
  auto b = y.begin();
//std::cerr << *a  << " " << *b << std::endl;
  while (a != x.end() && b != y.end()) {
    //std::cerr << *a <<" "<< *b <<std::endl;
    if (*a < *b) {
      ++a;
    } else if (*b < *a) {
      ++b;
    } else {
      v.push_back(*a);
      ++a;
      ++b;
    }

  }
  return v;
}

valueT ReadProcessor::findAnchor(const char* s, keyT& lk, int& pos, int len, int &rep_counts) {
  keyT re;
  valueT v;
  int by = 1;
  while ( pos + k + by <= len ) {
    re = kc.NextKey(lk, s, pos, by);
    v = index.othello->queryInt(re);
    //std::cerr << v << " " ;
    if (validValue(v)) {
      //std::cerr << std::endl;
      return v;
    }
    if (v == 0) {
      rep_counts++;
    }
  }
  return -1;
}

std::vector<valueT> ReadProcessor::ChewSeqMM(const char* s, int r_len) {
  // find the anchor gene, then jump,
  // if the next one is not anchor gene or rep
  // when to jump?
  //      1.find one alien kmer, jump ahead for 20 bases(1 mismatch)?
  //      2.find one gene with 5 block size, jump 20 bases,
  //             if mismatch found, go back to previous postion, continue move
  //                                by one base until found the mismatch, then
  //                                jump 20 bases
  //             otherwise, keep jumping.
  //std::vector<valueT>wk = {};
  // std::cerr << "breakl" << std::endl;
  int rep_count = 0;
  valueT v = 0;
  valueT lv = -1;
  int pos = 0;
  int lp = 0;
  int vCounter = 1;
  keyT key;
  keyT lastKey;
  int move = 0;
  std::map<valueT, int> walker;
  // std::map<valueT, size_t> count;
  kc.convert(s + pos, lastKey);
  // @P throwed the first kmer.
  lv = findAnchor(s, lastKey, pos, r_len, rep_count);
  lp = pos;
  stride = 1;
  // std::vector<valueT> Vs;
  // Vs.push_back(lv);
  int total = 0;
  //Not take care of info-kmer
  int left = pos ;
  while ( true ) {
    if (pos + stride + k > r_len) {
      break;
    }
    if (stride == k) {
      left = left + stride;
      pos = pos + stride;
      kc.convert(s + pos, lastKey);
      key = lastKey;
      stride = 1;
    } else {
      key = kc.NextKey(lastKey, s, pos);
    }
    //key = kc.NextKey(lastKey, s, pos);
    v = index.othello->queryInt(key);
    // Vs.push_back(v);
    if (validValue(v)) {
      total++;
      stride = 1;
      if (lv == v) {
        vCounter ++;

      } else {
        if ( vCounter > 1) {
          //std::cerr << "update" << std::endl;
          // count[lv] = count[lv] + vCounter;
          walker[lv] = walker[lv] + vCounter * vCounter;
        }
        vCounter = 1;
        lv = v;
        lp = pos;
      }
    } else {
      if (lv == v && v == 0) {
        rep_count++;
      } else {
        stride = 1;
      }

      if ( vCounter > 1) {
        //std::cerr << "update" << std::endl;
        walker[lv] = walker[lv] + vCounter * vCounter;
        // count[lv] = count[lv] + vCounter;
      }
      lv = v;
      lp = pos;
      vCounter = 1;
    }
  }

  if (lv == v && validValue(v) && vCounter > 1) {
    // count[lv] = count[lv] + vCounter;
    walker[lv] = walker[lv] + vCounter * vCounter;
  }
  // std::cerr << s << std::endl;
  // for (auto V : Vs) {
  //   std::cerr << V << " ";
  // }
  // std::cerr << std::endl;

  if (walker.empty()) {
    // std::cerr <<s << std::endl;
    // for (auto V : Vs) {
    //   std::cerr << V << " ";
    // }
    // std::cerr << std::endl;
    return {};
  }
  //Copy it into a vector.
  std::vector<std::pair<valueT, int>> vector( walker.begin(), walker.end() );

  // Sort the vector according to the word count in descending order.
  std::sort( vector.begin(), vector.end(),
             []( std::pair<const valueT, int> const & lhs, std::pair<const valueT, int> const & rhs )
  { return lhs.second > rhs.second; } );

  //std::cerr <<count[vector[0].first] << " " << total << std::endl;

  /* v1:
    if pass threshold, return the top 1 vector
    else
      do 1 round intersection
  */
  /*
  if (vector[0].second > 4 && count[vector[0].first] > total / 2 && total > (r_len - k + 1) / 2) {
    return GetValues(vector[0].first);
  } else if (vector[0].second > 4 && total > (r_len - k + 1) / 2) {

    if (vector.size() > 1) {
      std::vector <valueT>common = intersect(GetValues(vector[0].first), GetValues(vector[1].first));
      return common;
    } else {
      if(GetValues(vector[0].first).size()>1){
        return GetValues(vector[0].first);
      }else{
        return {};
      }
    }
  }
  else {
    return {};
  }*/
  /* v2:
  V2 run intersection on multiple sets without filter
  */
  /*
  if(total <=(r_len - k + 1) / 2){
    return {};
  }
  if (vector.size()==1){
    if (vector[0].second > 4 && count[vector[0].first] > total / 2 && total > (r_len - k + 1) / 2) {
      return GetValues(vector[0].first);
    }else{
      return {};
    }
  }else{
    std::vector<valueT> common = GetValues(vector[0].first);
    for(int i = 1; i < vector.size(); i++){
      common = intersect(common, GetValues(vector[i].first));
      if(common.empty()){
        return {};
      }
    }
    return common;
  }
  */
  // /*v3*/
  // /* require at least half useful information from the read*/
  // /* current implementation reduce the number of few counts, but too many large counts
  // due to the MM issues, will EM help? */
  // total = total + rep_count;
  // if (total <= (r_len - left - k + 1) / 2) {
  //   //std::cerr << "total " << total << ":" << (r_len - left - k + 1) / 2 << std::endl;
  //   return {};
  // }
  // /* if only 1 candidate, return */
  // if (vector.size() == 1) {
  //   /* need occur continuously */

  //   if (vector[0].second > 4) {
  //     return GetValues(vector[0].first);
  //   } else {
  //     return {};
  //   }
  // } else {
  //   /* if multiple candidate
  //      intersect and find the max covered candidate?
  //    */
  //   std::map<valueT,int> vote ;
  //   std::vector<valueT> common = GetValues(vector[0].first);
  //   for (auto &i:common){
  //     vote[i] = walker[vector[0].second];
  //   }
  //   for (int i = 1; i < vector.size(); i++) {
  //     common = intersect(common, GetValues(vector[i].first));
  //     for (auto &j:common){
  //       vote[j] =  vote[j] + walker[vector[i].second];
  //     }
  //     if (common.empty()) {
  //       // std::cerr << "emtpy" << std::endl;
  //       return {};
  //     }
  //   }
  //   /* only return single gene? (18262378)*/
  //   /* return all (18273618) */
  //   if (common.size() == 1)
  //     return common;
  //   else{
  //     // for (auto co:common){
  //     //   std::cerr << co << " " ;
  //     // }
  //     // std::cerr << std::endl;
  //     std::vector<std::pair<valueT, int>> vvote( vote.begin(), vote.end() );

  //     // Sort the vector according to the word count in descending order.
  //     std::sort( vvote.begin(), vvote.end(),
  //                []( std::pair<const valueT, int> const & lhs, std::pair<const valueT, int> const & rhs )
  //     { return lhs.second > rhs.second; } );
  //     std::vector<valueT> re;
  //     for(auto &v:vvote){
  //       if(v.second == vvote[0].second){
  //         re.push_back(v.first);
  //       }else{
  //         break;
  //       }

  //     }
  //     return re;
  //   }
  // }
  /*v4*/
  // total = total + rep_count;
  // if (total <= (r_len - left - k + 1) / 2) {
  //   //std::cerr << "total " << total << ":" << (r_len - left - k + 1) / 2 << std::endl;
  //   //return {};
  // }
  /* if only 1 candidate, return */
  if (vector.size() == 1) {
    /* need occur continuously */

    if (vector[0].second > 400) {
      return GetValues(vector[0].first);
    } else {
      return {};
    }
  } else {
    /* if multiple candidate
       intersect and find the max covered candidate?
     */
    // std::map<valueT, int> vote ;
    // std::map<valueT,int> counts;
    // for(auto &i:count){
    //   std::vector<valueT> tmp = GetValues(i.first);
    //   for(auto &j:tmp){
    //     counts[j]= counts[j] + i.second;
    //   }
    // }
    if (vector[0].second < 400){
      return {};
    }
    std::vector<valueT> common = GetValues(vector[0].first);
    // for (auto &i : common) {
    //   vote[i] = vector[0].second;
    // }
    std::vector<valueT> tmp;
    for (int i = 1; i < vector.size(); i++) {
      tmp = GetValues(vector[i].first);
      if (!common.empty())
        common = intersect(common, tmp);
      // for (auto &j : tmp) {
      //   vote[j] =  vote[j] + vector[i].second;
      // }
      // if (common.empty()) {
      //   // std::cerr << "emtpy" << std::endl;
      //   return {};
      // }
    }
    /* only return single gene? (18262378)*/
    /* return all (18273618) */
    if (common.size() == 1)
      return common;
    else {
      return {};
      // // for (auto co:common){
      // //   std::cerr << co << " " ;
      // // }
      // // std::cerr << std::endl;
      // std::vector<std::pair<valueT, int>> vvote( vote.begin(), vote.end() );
      // // for (auto &v:vvote){
      // //   std::cerr << v.first << " "<< v.second << std::endl;
      // // }
      // // Sort the vector according to the word count in descending order.
      // std::sort( vvote.begin(), vvote.end(),
      //            []( std::pair<const valueT, int> const & lhs, std::pair<const valueT, int> const & rhs )
      // { return lhs.second > rhs.second; } );
      // std::vector<valueT> re;
      // for (auto &v : vvote) {
      //   if (v.second == vvote[0].second && counts[vvote[0].first] > 50) {
      //     re.push_back(v.first);
      //   } else {
      //     break;
      //   }

      // }
      // return re;
    }

  }

}


void ReadProcessor::subset() {
  char* a = "GGATTACCATAGGATA\0";
  char* b = "CTCTGGTAGGAGTAGA\0";
  keyT A;
  kc.convert(a, A, 16);
  keyT B;
  kc.convert(b, B, 16);
  keyT C;
  const char* s1 = 0;
  for (int i = 0; i < seqs.size(); i++) {
    s1 = seqs[i].first;

    kc.convert(s1, C, 16);
    if (C == B || C == A) {
      writeFailedReads(
        names[i].first,
        s1,
        quals[i].first,
        names[i + 1].first,
        seqs[i + 1].first,
        quals[i + 1].first,
        paired);
    }
    i++;
  }

}

void MasterProcessor::processReads() {
  std::cerr << "Start thread" << endl;
  std::vector<std::thread> workers;
  std::cerr << "Start gene count on " << opt.threads << " threads" << std::endl;
  auto start = steady_clock::now();
  for (int i = 0; i < opt.threads; i++) {
    //std::cerr << "job: " << i << std::endl;
    workers.emplace_back(std::thread(ReadProcessor(index, opt, kc, *this)));
  }

  for (int i = 0; i < opt.threads; i++) {
    //std::cerr << "join job: " << i << std::endl;
    workers[i].join();
  }
  auto t_count = steady_clock::now();
  auto t_count_duration = duration_cast<minutes>(t_count - start);
  std::cerr << "Counting takes " << t_count_duration.count() << "mins" << std::endl;

  // process cells
  KmerCreater tmp(1, opt.bc_len, 1111);
  //KmerCreater umi_c(1, opt.umi_len, 1111);
  std::map<Cell, std::map<keyT, keyT>> cells_corrected;
  std::map<Cell, std::set<keyT>> cells_umis;
  std::map<Cell, int> cells_gene_readcount;
  int corr_count = 0;
  string strcurr;
  string strnew;
  std::map<keyT, int> cell_read_count;
  int nCellGene = 0;
  std::cerr << "Start UMI correction " << std::endl;

  std::vector <pair<Cell, std::map<keyT, uint32_t>>> cc;
  for (auto &v : cells){
    cc.push_back(make_pair(v.first, v.second));
    cell_read_count[v.first.cb] += v.second.size();
  }
  start = steady_clock::now();
  std::vector<int>ACTG = {0, 1, 2, 3};
  keyT oo = 3;
  // correct umi
  std::vector<pair<Cell, int>> umi_counts(cells.size());
  cells.clear();
  omp_set_num_threads(opt.threads);
  #pragma omp parallel for schedule(dynamic)
  //for (auto &c : cells) {// for each cb+geneid
  // for(std::map<Cell,std::map<keyT,uint32_t>>::iterator cgcount=std::begin(cells);
  //     cgcount != std::end(cells);cgcount++){
  // A,0 T,3 G,2 C,1
  // correct umis in c.second
  for (int x = 0; x < cc.size(); x++) {
    int currcount;
    keyT currkey;
    std::set<keyT> uniq_umis;
    // auto cgcount = cells.begin();
    // std::advance(cgcount, x);
    for (auto &umi : cc[x].second) {// for each umi
      // cell_read_count[c.first.cb] += umi.second;
      // cells_gene_readcount[c.first]  += umi.second;
      // for each umi
      currcount = umi.second;
      currkey = umi.first;
      //std::cerr << "thread " << threadNO << std::endl;
      for (auto i : ACTG) {
        for (int pos = 0; pos < opt.umi_len ; pos++) {
          // change char on each position
          keyT change_char = (currkey & (~(oo << ( pos * 2)))) | (i << pos * 2);

          //strnew=tmp.convertString(umi2.first);
          if (currkey == change_char) continue;
          if (HWgt1(currkey, change_char)) continue;
          auto iter = cc[x].second.find(change_char);
          if (iter == cc[x].second.end()) continue;
          if ((currcount < cc[x].second[change_char]) ||
              (currcount == cc[x].second[change_char] && currkey < change_char)) {
            currcount = cc[x].second[change_char];
            currkey = change_char;
            corr_count++;
          }
          // corr_count++;
          // currcount = umi2.second;
          // currkey = umi2.first;

          /* track the umi change */
          // cells_corrected[c.first][umi.first] = currkey;


        }

      }

      uniq_umis.insert(currkey);
    }
    umi_counts[x] = (std::make_pair(cc[x].first, uniq_umis.size()));

  }
  cc.clear();
  auto t_fixumi = steady_clock::now();
  auto t_fixumi_duration = duration_cast<minutes>(t_fixumi - start);
  std::cerr << "UMI correction takes " << t_fixumi_duration.count() << "mins" << std::endl;
  // for (auto &c : cell_read_count) {
  //   std::cerr << tmp.convertString(c.first) << " : " << c.second << std::endl;
  // }
  std::cerr << "number of cells" << cell_read_count.size() << std::endl;
  std::cerr << "corrected count " << corr_count << std::endl;

  bool output_readcount = false;
  if (output_readcount) {
    for (auto &c : cells_gene_readcount) {
      int cn = c.second;
      auto it = cell_count.find(c.first.cb);
      if (it == cell_count.end()) {
        GeneCounter gc = GeneCounter(index, opt);
        gc.counts[c.first.gid - 1] = cn;
        cell_count.insert(pair<keyT, GeneCounter>(c.first.cb, gc));
      } else {
        it->second.counts[c.first.gid - 1] = cn;
      }
    }//
  } else {

    // umi count
    for (auto &c : umi_counts) {

      if(cell_read_count[c.first.cb] < 100) continue;
      /* output umi correction */
      // for (auto &u : c.second) {
      //   std::cerr << c.first.gid << "\t" << umi_c.convertString(u) << std::endl;
      // }
      int cn = c.second;
      if (cn > 0) nCellGene++;
      auto it = cell_count.find(c.first.cb);
      if (it == cell_count.end()) {
        GeneCounter gc = GeneCounter(index, opt);
        gc.counts[c.first.gid - 1] = cn;
        cell_count.insert(pair<keyT, GeneCounter>(c.first.cb, gc));
      } else {
        it->second.counts[c.first.gid - 1] = cn;
      }

    }
  }//
  // std::cerr << "read per cell" << std::endl;
  // for (auto c:cell_read_count){
  //   std::cerr << tmp.convertString(c.first) << " : " << c.second << std::endl;
  // }

  // Dump matrix to mtk
  std::cerr << "dump count matrix" << std::endl;
  std::ofstream quant_mat;
  std::ofstream gene_list;
  std::ofstream bc_list;

  quant_mat.open("matrix.mtx");
  gene_list.open("genes.tsv");
  bc_list.open("barcodes.tsv");
  quant_mat << "%%MatrixMarket matrix coordinate integer general\n%\n";
  quant_mat << index.num_genes << ' ' << cell_count.size() << ' ' << nCellGene << '\n';

  string bcid;
  for ( auto &c : cell_count) {
    bcid = tmp.convertString(c.first);
    bc_list << bcid << "-1\n";
  }

  for (int i = 0; i < index.num_genes; i++) {
    gene_list << index.geneids[i] << "\t" << index.geneids[i] << '\n';
  }

  //test write
  // for( int i = 0; i < index.num_genes; i++){
  //   for( auto &c : cell_count){
  //     quant_mat << '\t' << 1;
  //   }
  //   quant_mat << '\n';
  // }

  //get umi count for each gene

  for (int i = 0; i < index.num_genes; i++) {

    int j = 0;
    for (auto &c : cell_count) {
      if (c.second.counts[i] > 0)
        quant_mat << i + 1 << ' ' << j + 1 << ' ' << c.second.counts[i] << "\n";
      j++;
    }
  }
}

/* backup
void MasterProcessor::processReads() {

// start worker threads
  if (!opt.batch_mode) {
    std::cerr << "Start thread" << endl;
    std::vector<std::thread> workers;
    for (int i = 0; i < opt.threads; i++) {
      std::cerr << "job: " << i << std::endl;
      workers.emplace_back(std::thread(ReadProcessor(index, opt, kc, *this)));
    }
    std::cerr << "Start thread join" << endl;
    // let the workers do their thing
    for (int i = 0; i < opt.threads; i++) {
      std::cerr << "join job: " << i << std::endl;
      workers[i].join(); //wait for them to finish
    }
    std::cerr << "Start thread join done" << endl;


    //add repcount
    if (!opt.scRNA) {
      for (int i = 0; i < repReads.size(); i++) {
        std::vector <valueT> g = repReads[i];
        double gtotal = 0.0;

        for (int j = 0; j < g.size(); j++) {
          gtotal = gc.counts[g[j] - 1] + gtotal;
        }
        if (gtotal != 0 ) {
          for (auto x : g) {
            gcrep.counts[x - 1] = gcrep.counts[x - 1] + gc.counts[x - 1] / gtotal;
          }
        }

      }
      for (int i = 0; i < gc.counts.size(); i++) {
        if (gc.counts[i] != 0) {
          std::cout << index.geneids[i] << '\t' << gc.counts[i] << '\t' <<
                    gc.counts[i] + gcrep.counts[i] << std::endl;
          //(gc.counts[i]/nummapped)*(gcrep.counts[i]) * 1.0 << '\t' << gcrep.counts[i] << std::endl;
        }
      }
      std::cerr << "Total reads:  " << numreads << std::endl;
      std::cerr << "Total mapped: " << nummapped << std::endl;
      for ( int i = 0; i < JCount.size() ; i++) {
        if (JCount[i] != 0) {
          std::cerr << "Jump " << i << " = " << JCount[i] << std::endl;
        }
      }
    } else {
      std::cerr << "proces cells " << cells.size() << std::endl;
      // process cells
      KmerCreater tmp(1, 16, 1111);
      std::map<Cell, std::map<keyT,keyT>> cells_corrected;
      std::map<Cell, std::set<keyT>> cells_umis;
      std::map<Cell, int> cells_gene_readcount;
      int corr_count = 0;
      string strcurr;
      string strnew;
      std::map<keyT, int> cell_read_count;
      for (auto &c : cells) {

        // A,0 T,3 G,2 C,1
        // correct umis in c.second
        int currcount;
        keyT currkey;
        //#bool stop = (c.second.size() > 10);
        bool stop = false;
        for (auto &umi : c.second) {
          read_p_cell[c.first.cb] += umi.second;
          cells_gene_readcount[c.first]  += umi.second;
          // for each umi
          currcount = umi.second;
          currkey = umi.first;

          // compare to all umis
          //strcurr= tmp.convertString(currkey);

          for (auto &umi2 : c.second){

            //strnew=tmp.convertString(umi2.first);
            if (currkey == umi2.first) continue;
            if (HWgt1(currkey,umi2.first)) continue;
            if (stop) {
              std::cerr << strcurr << "-" << strnew << " " << currcount << " " << umi2.second << std::endl;
            }
            if ((currcount < umi2.second) ||
                (currcount == umi2.second && currkey <umi2.second)){
              currcount = umi2.second;
              currkey = umi2.first;
              corr_count++;
            }
            // corr_count++;
            // currcount = umi2.second;
            // currkey = umi2.first;
          }
          cells_corrected[c.first][umi.first] = currkey;
          cells_umis[c.first].insert(currkey);
        }

        // for (auto &x : cells_umis[c.first]){
        //   strcurr = tmp.convertString(x);
        //    if (stop) std::cerr << strcurr << " " << std::endl;
        // }
        if (stop) break;

      }
      for (auto &c:read_p_cell){
        std::cerr << tmp.convertString(c.first) << " : " << c.second << std::endl;
      }
      std::cerr << "number of cells" << read_p_cell.size() << std::endl;

      std::cerr << "corrected count " << corr_count << std::endl;
        //std::cerr << c.first.cb << " " << c.first.gid <<" " << cn << std::endl;
      bool output_readcount = false;
      if(output_readcount){
        for (auto &c: cells_gene_readcount){
          int cn = c.second;
          auto it = cell_count.find(c.first.cb);
          if (it == cell_count.end()) {
            GeneCounter gc = GeneCounter(index, opt);
            gc.counts[c.first.gid - 1] = cn;
            cell_count.insert(pair<keyT, GeneCounter>(c.first.cb, gc));
          } else {
            it->second.counts[c.first.gid - 1] = cn;
          }
        }//
      }else{

      // umi count
        for (auto &c : cells_umis){

          if(read_p_cell[c.first.cb] < 100) continue;
          int cn = c.second.size();
          auto it = cell_count.find(c.first.cb);
          if (it == cell_count.end()) {
            GeneCounter gc = GeneCounter(index, opt);
            gc.counts[c.first.gid - 1] = cn;
            cell_count.insert(pair<keyT, GeneCounter>(c.first.cb, gc));
          } else {
            it->second.counts[c.first.gid - 1] = cn;
          }

        }
      }//
      // std::cerr << "read per cell" << std::endl;
      // for (auto c:read_p_cell){
      //   std::cerr << tmp.convertString(c.first) << " : " << c.second << std::endl;
      // }
      std::cerr << "dump count matrix" << std::endl;
      std::ofstream quant_mat;
      quant_mat.open("quant_mat.tsv");

      //test write
      // for( int i = 0; i < index.num_genes; i++){
      //   for( auto &c : cell_count){
      //     quant_mat << '\t' << 1;
      //   }
      //   quant_mat << '\n';
      // }

      //get umi count for each gene
      int j;
      string header = "gene_id";
      string bcid;
      for (int i = 0; i < index.num_genes; i++) {
        if(i == 0){
          for( auto &c: cell_count){
            bcid = tmp.convertString(c.first);
            header = header + "\t" + bcid;
          }
          quant_mat << header ;
          quant_mat << "\n";
        }
        string tmp = index.geneids[i];
        quant_mat << tmp;
        for (auto &c : cell_count) {
          // if(c.second.counts[i]!=0)
            quant_mat << '\t' << c.second.counts[i];
          //tmp = tmp + '\t' + std::to_string(c.second.counts[i]);
        }
        quant_mat << "\n";
      }
      // string buffer;
      // int count = 0;
      // for (int i = 0; i < index.num_genes; i++) {
      //   if(i == 0){
      //     for(int j=0; j < 8; j++){
      //       //bcid = tmp.convertString(c.first);
      //       bcid = "DFFD";
      //       header = header + "\t" + bcid;
      //     }
      //     quant_mat << header ;
      //     quant_mat << "\n";
      //   }

      //   if (count == 100000  ) {
      //     quant_mat << buffer;
      //     buffer.clear();
      //   }
      //   count ++;

      //   string tmp = index.geneids[i];
      //   buffer = buffer + tmp;
      //   for (int j=0; j < 8; j++) {
      //     buffer = buffer + "\t" + "1";//std::to_string(c.second.counts[i]);
      //     //tmp = tmp + '\t' + std::to_string(c.second.counts[i]);
      //   }
      //   buffer = buffer + "\n";
      // }
      // if ((index.num_genes-1)%100000 == 0){
      //   quant_mat << buffer;
      //   buffer.clear();
      // }
    }

  }
}

*/

void MasterProcessor::update(const std::vector<valueT>& g,
                             const std::vector<std::vector<valueT>>&gv,
                             std::vector<int> &jump_count, int numseqs,
                             std::vector<std::pair<Cell, keyT>>& cell) {
  // acquire the writer lock
  std::lock_guard<std::mutex> lock(this->writer_lock);
  numreads = numreads + numseqs;
  for (int i = 0; i < jump_count.size(); i++) {
    JCount[i] = JCount[i] + jump_count[i];
  }
  if (!opt.scRNA) {
    for (int i = 0; i < g.size(); i++) {
      gc.counts[g[i] - 1]++ ;
      nummapped ++;
    }
    // for (int i = 0; i < gv.size(); i++) {
    //   gcrep.counts[gv[i]-1]++;
    //   //nummapped ++;
    // }
    //Append(repReads, gv);

  } else {
    // update cells
    //std::cerr << "Size " << cells.size() << " Cell " << cell.size()<< std::endl;
    for (auto &c : cell) {
      //std::cerr << c.first.cb << " " << c.first.gid << " " << cells[c.first] << std::endl;
      cells[c.first][c.second]++;
      //std::cerr << cells[c.first][c.second] << std::endl;
    }

  }
}


void ReadProcessor::CELL() {
  int l1, l2;
  std::vector<int> failed;
  int before;
  std::vector<std::pair<valueT, int>> r1;
  std::vector<std::pair<valueT, int>> r2;
  const char* s1 = 0;
  const char* s2 = 0;
  keyT cb, umi;
  numassign = 0;
// iterate seqs
  for (int i = 0; i < seqs.size(); i++) {

    before++;
    s1 = seqs[i].first;
    l1 = seqs[i].second;
    i++;
    s2 = seqs[i].first;
    l2 = seqs[i].second;

    r1.clear();
    r2.clear();
    kc.convert(s1, cb, bc_len);
    kc.convert(s1 + bc_len, umi, umi_len);//v3
    //kc.convert(s1 + 16, umi, 10);//v2

    // kc.convert(s1, cb, 6);
    // kc.convert(s1 + 6, umi, 10);
    // ATTGTTGATCTTCACT
    if (index.mapbc.find(cb) == index.mapbc.end()) {
      // if (index.barcodeA->queryInt(cb) != index.barcodeB->queryInt(cb)) {
      //   // std::cerr << index.barcodeA->queryInt(cb) << " " << index.barcodeB->queryInt(cb) << std::endl;
      //std::cerr << "not exist" << std::endl;
      continue;
    }
    std::vector<valueT> gr1;
    gr1 = ChewSeqMM(s2, l2);
    //if(cb == 1644905209){
    // writeFailedReads(names[i - 1].first, s1, quals[i - 1].first, names[i].first, s2, quals[i].first, paired);
    //}
    //std::cerr << s2 << std::endl;
    if (gr1.size() == 1) {
      numassign++;
      Cell c;
      c.cb = cb;
      // std::cerr << "Cb" << cb << std::endl;
      c.gid = gr1[0];
      // if(c.gid==19204){
      //   writeFailedReads(names[i - 1].first, s1, quals[i - 1].first, names[i].first, s2, quals[i].first, paired);
      //   writeFailed(gr1, gr1, names[i-1].first, s1, names[i].first, s2, paired);
      // }
      // if (c.gid == 1103){
      //  std::cerr << names[i-1].first << "\t" << names[i].first << std::endl;
      // }
      cell.push_back({c, umi});
    } else {
      //std::cerr << gr1.size() << endl;
      if (write_failed) {

        writeFailed(gr1, gr1, names[i - 1].first, s1, names[i].first, s2, paired);
        writeFailedReads(names[i - 1].first, s1, quals[i - 1].first, names[i].first, s2, quals[i].first, paired);
      }
    }
  }
  numseqs = numassign;
}

ReadProcessor::ReadProcessor(
  const OthelloIndex & index, const Opts & opt, KmerCreater & kc, MasterProcessor & mp):
  paired(!opt.single_end), index(index), mp(mp), kc(kc), fusion(opt.fusion),
  k(opt.k), all(opt.all_kmer), stride(opt.stride), write_failed(opt.output_failed),
  umi_len(opt.umi_len), bc_len(opt.bc_len) {
// initialize buffer
  bufsize = mp.bufsize;
  buffer = new char[bufsize];
  seqs.reserve(bufsize / 50);
//kc.init(stride,k); If I do this, not working
  clear();
}


/* find the next valid kmer in 3mer window */
valueT ReadProcessor::findAnchor(const char* s, keyT & lk, int& pos, int len) {
  keyT re;
  valueT v;
  while ( pos + k + 1 <= len ) {
    re = kc.NextKey(lk, s, pos, 1);
    v = index.othello->queryInt(re);
    if (validValue(v)) {
      return v;
    }
  }
  return -1;
}


// std::vector<valueT> ReadProcessor::ChewSeq(const char* s, int r_len) {
// // find the anchor gene, then jump,
// // if the next one is not anchor gene or rep
// // when to jump?
// //      1.find one alien kmer, jump ahead for 20 bases(1 mismatch)?
// //      2.find one gene with 5 block size, jump 20 bases,
// //             if mismatch found, go back to previous postion, continue move
// //                                by one base until found the mismatch, then
// //                                jump 20 bases
// //             otherwise, keep jumping.
//   std::map <valueT, GeneLog> walker;
// //std::unordered_map < valueT,int > wk;
//   std::vector<valueT>wk = {};
//   int pos = 0;
//   int jump = 0;
//   valueT val;
//   valueT before;
//   valueT after;
//   int bsize = 1;
//   int rep = 0;
//   keyT key;
//   keyT lastKey;
//   int move = 0;

// //std::cerr << kc.stride << " " << kc.k << std::endl;
//   kc.convert(s + pos, lastKey);
// //std::cerr << pos << " " <<lastKey << std::endl;
// // @P throwed the first kmer.
//   val = findAnchor(s, lastKey, pos, r_len);
// // while(pos + stride + k <= r_len ){
// //   key = kc.NextKey(lastKey, s, pos);
// //   val = index.othello->queryInt(key);
// //   // //bsize = updateWalker(walker,val,pos,rep,k);
// //   if (validValue(val)){
// //    //bsize = updateWalker(walker,val,pos,rep,k);
// //    wk.push_back(val);
// //     // if(val == before){
// //     //   wk[val]++;
// //     // }else{
// //     //   before = val;
// //     // }
// //   }

// // }

// //  unordered_set<int> ss;
// // for (auto i : wk)
// //     ss.insert(i);
// // wk.assign( ss.begin(), ss.end() );

//   bsize = updateWalker(walker, val, pos, rep, 1);
//   while ( true ) {
//     //std::cerr << val << " " << bsize  ;
//     if (bsize > 1 && pos + stride + k <= r_len) {
//       jump++;
//       // no overlap
//       //std::cerr << " jump ";
//       key = kc.NextKey(lastKey, s, pos);
//       val = index.othello->queryInt(key);
//       move = stride;
//     } else if (pos + 3 + k <= r_len) {
//       val = findAnchor(s, lastKey, pos, r_len);
//       //std::cerr << " anchor ";
//       move = 3;
//     } else {
//       //std::cerr << std::endl;
//       break;
//     }

//     //printf("p:%d, b:%d, v:%d\n",pos,bsize,val);
//     if (validValue(val)) {
//       bsize = updateWalker(walker, val, pos, rep, move);
//     } else {
//       bsize = 0;
//     }
//   }

// // //std::cerr <<jump << std::endl;
//   if (walker.empty()) {
//     //std::cerr << "empty" << std::endl;
//     return {};
//   }
// //auto start = high_resolution_clock::now();
//   std::vector<GeneLog> top_list;
//   for ( auto &m : walker ) {
//     //std::cerr << m.second.maxBlock;
//     if ((m.second.maxBlock > 1)) {
//       top_list.push_back(m.second);
//     }
//   } //end for
//   int n_genes = top_list.size();


//   if (n_genes == 0) {

//     return {};
//   } else {
//     std::vector<valueT> uniq;

//     std::vector<valueT> v = GetValues(top_list[0].gv);

//     for (int i = 1; i < n_genes; i++ ) {
//       v = intersect(v, GetValues(top_list[i].gv));
//       if (v.empty()) {
//         return {};
//       }
//     }
//     jump_count[jump]++;
//     return v;

//   }
// }

std::vector<valueT> ReadProcessor::ChewSeq(const char* s, int r_len) {
  // find the anchor gene, then jump,
  // if the next one is not anchor gene or rep
  // when to jump?
  //      1.find one alien kmer, jump ahead for 20 bases(1 mismatch)?
  //      2.find one gene with 5 block size, jump 20 bases,
  //             if mismatch found, go back to previous postion, continue move
  //                                by one base until found the mismatch, then
  //                                jump 20 bases
  //             otherwise, keep jumping.
  //std::vector<valueT>wk = {};
  int pos = 0;
  int jump = 0;
  valueT val;
  valueT last_val = -1;
  int bsize = 1;
  int rep = 0;
  keyT key;
  keyT lastKey;
  int move = 0;
  std::map<valueT, size_t> walker;
  //walker.reserve(10);

  //std::cerr << kc.stride << " " << kc.k << std::endl;
  kc.convert(s + pos, lastKey);
  //std::cerr << pos << " " <<lastKey << std::endl;
  // @P throwed the first kmer.
  val = findAnchor(s, lastKey, pos, r_len);
  // while(pos + stride + k <= r_len ){
  //   key = kc.NextKey(lastKey, s, pos);
  //   val = index.othello->queryInt(key);
  //   // //bsize = updateWalker(walker,val,pos,rep,k);
  //   if (validValue(val)){
  //    //bsize = updateWalker(walker,val,pos,rep,k);
  //    wk.push_back(val);
  //     // if(val == before){
  //     //   wk[val]++;
  //     // }else{
  //     //   before = val;
  //     // }
  //   }

  // }

//  unordered_set<int> ss;
  // for (auto i : wk)
  //     ss.insert(i);
  // wk.assign( ss.begin(), ss.end() );
  if (validValue(val)) {
    if (last_val == val) {
      walker[val]++;
      bsize = 1;
    } else {
      last_val = val;
      bsize = 0;
    }
    //walker[val]++;

  } else {
    bsize = 0;
  }
  //bsize = updateWalker(walker, val, pos, rep, 1);
  while ( true ) {
    //std::cerr << val << " " << bsize  ;
    if (bsize == 1 && pos + stride + k <= r_len) {
      jump++;
      // no overlap
      //std::cerr << " jump ";
      key = kc.NextKey(lastKey, s, pos);
      val = index.othello->queryInt(key);
    } else if (pos + 1 + k <= r_len) {
      val = findAnchor(s, lastKey, pos, r_len);
    } else {
      //std::cerr << std::endl;
      break;
    }

    //printf("p:%d, b:%d, v:%d\n",pos,bsize,val);
    if (validValue(val)) {
      if (val == last_val) {
        walker[val]++;
        bsize = 1;
      } else if (intersect(GetValues(val), GetValues(last_val)).size() != 0) {
        walker[val]++;
        walker[last_val]++;
        bsize = 1;
      }
      else {
        last_val = val;
        bsize = 0;
      }


    } else {
      bsize = 0;
    }
  }

  // find the longest consecutive valueT in the vector
  //valueT re = maxRepeating(cand);
  //return maxRepeating(cand);
  //std::cerr << re << std::endl;
  // if ( re != 0) {
  //   return {re};
  // } else {
  //   return {};
  // }



  // std::vector<std::pair<valueT, int>> top_list(2);
  // std::partial_sort_copy(wk.begin(),
  //                        wk.end(),
  //                        top_list.begin(),
  //                        top_list.end(),
  //                        [](std::pair<const valueT, int> const& l,
  //                           std::pair<const valueT, int> const& r)
  //                        {
  //                            return l.second > r.second;
  //                        });
  // if (top_list[0].first!=0){
  //   return {top_list[0].first};
  // }else{
  //   return {};
  // }
  if (walker.empty()) {
    //std::cerr <<"empty" << std::endl;
    return {};
  }
  std::map <valueT, size_t> kcount;
  for (auto &w : walker) {
    if (w.first > index.num_genes) {
      for (auto &vs : GetValues(last_val)) {
        kcount[vs] += w.second;
      }
    } else {
      kcount[w.first] = w.second;
    }
  }
  //Copy it into a vector.
  std::vector<std::pair<valueT, size_t>> vector( kcount.begin(), kcount.end() );

  // Sort the vector according to the word count in descending order.
  std::sort( vector.begin(), vector.end(),
             []( std::pair<const valueT, size_t> const & lhs, std::pair<const valueT, size_t> const & rhs )
  { return lhs.second > rhs.second; } );
  if (vector[0].second > 5) {
    return {vector[0].first};
  } else {
    return {};
  }
  // // //std::cerr <<jump << std::endl;

  // //auto start = high_resolution_clock::now();
  // std::vector<valueT> top_list;
  // int countg = 0;
  // for ( auto &m : vector ) {
  //   // countg++;
  //   //std::cerr << m.first << ":" << m.second << " ";
  //   //std::cerr << m.second.maxBlock;
  //   if (m.second > 2){
  //     top_list.push_back(m.first);
  //   }
  // } //end for
  // //std::cerr << std::endl;
  // // if (countg == 0){
  // //   std::cerr << s << std::endl;
  // // }
  // // if (countg==10){
  // //   for( auto i : vector ){
  // //     std::cerr << i.first << ":" << i.second << " ";
  // //   }
  // //   std::cerr << std::endl;
  // // }
  // int n_genes = top_list.size();


  // if (n_genes == 0) {

  //   return {};
  // } else {
  //   std::vector<valueT> v = GetValues(top_list[0]);

  //   // for (int i = 1; i < n_genes; i++ ) {
  //   //   v = intersect(v, GetValues(top_list[i]));
  //   //   if (v.empty()) {
  //   //     return {};
  //   //   }
  //   // }
  //   // jump_count[jump]++;
  //   return v;

  // }

}

/* Process sgRNA
 step 1: find the target sequence
 step 2: find the transRNA
*/
std::vector<valueT> ReadProcessor::Teeth(const char* s, int r_len) {

  int stride = 1;
  int jump = 0;
  keyT kmer;
  keyT kmerTag;
  std::vector <valueT> info;
  KmerCreater kc(stride, k, r_len);
// KmerCreater kctag(stride, 5,7);
// kctag.convert(tag,kmerTag);
// kmerTag = kctag.minSelfAndRevcomp(kmerTag);
  keyT mask = kc.bitmask();
  std::map <valueT, int> walker;
  int pos = 0;
  valueT re = 0;
  int bsize;
  int rep = 0;
#ifdef DEBUG_ASSIGN
  std::cerr << "In teeth" << std::endl;
#endif
  int weight = 100;
  valueT cur = 0;
  while (pos + k <= r_len && weight != 0) {
    re = firstTarget(kc, mask, s, r_len, pos, weight);
    if (re == 0) {
      pos++;
      continue;

    } else {
      cur = re;
    }
    //std::cerr << pos << " " ;
    pos++;
  }
//std::cerr << weight << " " << re <<std::endl;
//std::cerr << "val " << cur << " " << weight << std::endl;
  if (cur == 0) {
    //std::cerr <<"empty" << std::endl;
    jump_count[jump]++;
    return {};
  } else {

    return {cur};
  }
}



/* Process mRNA seq*/
std::vector<valueT> ReadProcessor::Teeth(const char* s, int r_len,
    int stride) {
// find the anchor gene, then jump,
// if the next one is not anchor gene or rep
// when to jump?
//      1.find one alien kmer, jump ahead for 20 bases(1 mismatch)?
//      2.find one gene with 5 block size, jump 20 bases,
//             if mismatch found, go back to previous postion, continue move
//                                by one base until found the mismatch, then
//                                jump 20 bases
//             otherwise, keep jumping.
  int jump = 0;
  keyT kmer;
  std::vector <valueT> info;
  KmerCreater kc(stride, k, r_len);
  bool jump_fwd;
  keyT mask = kc.bitmask();
  std::map <valueT, GeneLog> walker;
  int pos = 0;
  valueT val;
  int bsize;
  int rep = 0;
  keyT lastKey;
// anchor the first valide pos
  lastKey = findMappedGene(kc, mask, s, r_len, pos, walker, all);
#ifdef PRINT_DEBUG
  std::cerr << "Find mapped1 jump?" << jump_fwd << " pos" << pos << std::endl;
#endif
  while ( pos < r_len - k ) {
    pos = pos - 1 + stride;
    jump++;
    // check if position in out of the boundary
    if (pos > r_len) {
      pos = pos - stride + 1;
      lastKey = findMappedGene(kc, mask, s, r_len, pos, walker, all);
      break;
    }

    kc.convert(s + pos, kmer);
    kmer = kc.minSelfAndRevcomp(kmer);
    val = index.othello->queryInt(kmer);

    if (validValue(val, rep)) {
      bsize = updateWalker(walker, val, pos, rep, k);
      pos++;
      if ( bsize == 1 ) {
        lastKey = findMappedGene(kc, mask, s, r_len, pos, walker, true);
      }
    } else {
      pos++;
      lastKey = findMappedGene(kc, mask, s, r_len, pos, walker, true);
    }


  }

  /*
  while(jump_fwd && !all){
    pos = pos - 1 + stride;
    jump++;
    //std::cerr << "Jump pos " << pos << std::endl;
    if( pos >= r_len){
      // unable to jump by k, get kmer incrementally? or only the last one?
      #ifdef PRINT_DEBUG
      std::cerr<<"Find mapped2 ,pos: " << pos << std::endl;
      #endif
      pos = pos - stride + 1;
      jump_fwd = findMappedGene(kc,mask,s,r_len,pos,walker, all);
      jump_fwd = false;
      continue;
    }
    #ifdef PRINT_DEBUG
    std::cerr << "Jump " << pos << endl;
    #endif
    // process new position
    kc.convert(s + pos,kmer);
    kmer = kc.minSelfAndRevcomp(kmer);
    val = index.othello->queryInt(kmer);

    if(validValue(val,rep)){
      bsize = updateWalker(walker,val,pos,rep,k);
      if(bsize == 1){
        // A new gene, tmp solution
        // discrete checking the rest
        //findMappedGene(kc,mask,s,r_len,pos+1,walker,true);
        //jump_fwd = false;
        // or let it choose
        // start from the next position
        jump_fwd = findMappedGene(kc,mask,s,r_len,pos,walker,all);
      }
    }else{
      // is rep or alien kmer
      // discrete checking the rest from this position
      //findMappedGene(kc,mask,s,r_len,pos,walker,true);
      //jump_fwd = false;
      // start from the next position
      jump_fwd = findMappedGene(kc,mask,s,r_len,pos,walker,all);
    }
  }*/
  jump_count[jump]++;
  if (walker.empty()) {
    //std::cerr <<"empty" << std::endl;
    return {};
  }
//auto start = high_resolution_clock::now();
  std::vector<GeneLog> top_list;
  for ( auto &m : walker ) {
    // printf("last_p %d, first_p %d total %d n_rep %d\n",m.second.last_p,
    //  m.second.first_p,m.second.tot,m.second.n_rep);
    //std::cerr << "block " << m.second.maxBlock
    //<< "mismatch " << m.second.estMis(1,2,k) << std::endl;

    // check:
    //  1. total kmer + rep kmer, currently only include before or inbetween
    //  2. check continuous block size
    //  3. possible mismatch
    // std::cerr << m.second.gv <<" "<<((m.second.tot + m.second.n_rep) > ( r_len/3 - k)) <<" "<< (m.second.maxBlock > 5) << " "<< m.second.estMis(1,2,k) << std::endl;
    // if(((m.second.tot + m.second.n_rep) > ( r_len/3 - k)) && (m.second.maxBlock > 5) && m.second.estMis(1,2,k)){
    //   //will filter mismatch over 1
    //   top_list.push_back(m.second);
    // }
#ifdef PRINT_DEBUG
    std::cerr << m.second.gv << " " << m.second.tot <<
              " "  << m.second.n_rep  <<
              " " << m.second.first_p <<
              " " << m.second.maxBlock <<
              " " << m.second.last_p <<
              " " << m.second.n_mismatch <<
              std::endl;
#endif
    if ((m.second.maxBlock > 2)) {
      top_list.push_back(m.second);
    }
  } //end for
  int n_genes = top_list.size();
  if (n_genes == 0) {

    return {};
  } else {
    std::vector<valueT> uniq;

    std::vector<valueT> v = GetValues(top_list[0].gv);

    for (int i = 1; i < n_genes; i++ ) {
      v = intersect(v, GetValues(top_list[i].gv));
      if (v.empty()) {
        //std::cerr <<"empty1" << std::endl;
        return {};
      }
    }
// sort(top_list.begin(), top_list.end(), [&](std::pair<const valueT, GeneLog> a,
//  std::pair<const valueT, GeneLog> b)
//       {
//         if (a.second.pos==b.second.pos) {
//           return a.second.tot > b.second.tot;
//         } else {
//           return a.second.pos<b.second.pos;
//         }
//       });
    // auto stop = high_resolution_clock::now();
    // auto duration = duration_cast<microseconds>(stop - start);
    // std::cerr << duration.count() << std::endl;
    return v;

  }
}

void ReadProcessor::Monster() {

  /*
    1. iterate through reads, extract kmers and assign kmer to gene
    2. assign read to gene
    counts: vector of gv key
  */
  int l1, l2;
  std::vector<int> failed;
  int before;
  std::vector<std::pair<valueT, int>> r1;
  std::vector<std::pair<valueT, int>> r2;
  const char* s1 = 0;
  const char* s2 = 0;
// iterate seqs
  for (int i = 0; i < seqs.size(); i++) {

    before = numassign;
    numseqs++;
    s1 = seqs[i].first;

    l1 = seqs[i].second;
    if (paired) {
      i++;
      s2 = seqs[i].first;
      l2 = seqs[i].second;
    }
#ifdef PRINT_DEBUG
    std::cerr << s1 << std::endl;
    std::cerr << s2 << std::endl;
#endif
    r1.clear();
    r2.clear();

    std::vector<valueT> gr1;
    std::vector<valueT> gr2;
    bool gRNA = false;
    if (gRNA) {
      /* Process sgRNA */
      gr1 = Teeth(s1, l1);
      if (paired) {
        int c0, c1;

        char*s2T = strdup(s2);
        for (int j = 0; j < l2 >> 1; ++j) { // reverse complement sequence
          c0 = comp_tab[(int)s2T[j]];
          c1 = comp_tab[(int)s2T[l2 - 1 - j]];
          s2T[j] = c1;
          s2T[l2 - 1 - j] = c0;
        }
        if (l2 & 1) // complement the remaining base
          s2T[l2 >> 1] = comp_tab[(int)s2T[l2 >> 1]];

        gr2 = Teeth(s2T, l2);
        free(s2T);
      }
    } else {
      gr1 = ChewSeq(s1, l1);
      if (paired) {
        gr2 = ChewSeq(s2, l2);
      }
    }

    std::vector<valueT> re;

    if (gr1.empty() && !gr2.empty()) {

      re = gr2;
    } else if (gr2.empty() && !gr1.empty()) {

      re = gr1;
    } else if (!gr2.empty() && !gr1.empty()) {
      //std::cerr << "monster" << std::endl;

      re = intersect(gr1, gr2);
      //std::cerr << "monster-end" << std::endl;
    } else {
      re.clear();
    }
    if (re.empty() ) {

      if (write_failed) {
        if (numassign - before == 0) {
          writeFailed(gr1, gr2, names[i - 1].first, s1, names[i].first, s2, paired);
          writeFailedReads(names[i - 1].first, s1, quals[i - 1].first, names[i].first, s2, quals[i].first, paired);
        }

      }
      continue;
    } else {
      numassign++;
      //std::cerr << re[0] << std::endl;
      //double c = 1.0/re.size();
      if (re.size() == 1) {
        genes.push_back(re[0]);
      } else {
        repgenes.push_back(re);
        // for(auto r:re){
//     //std::cerr << "assign " << r << std::endl;
//       repgenes.push_back(r);
//     //genecounts.push_back(c);
//     }
      }

    }
    /*
    if (gr1.empty()==0 || gr2.empty()==0){
      std::vector<GeneLog> sol;
      sol.clear();
      bool checkFusion=false;
      bool ispair=false;
      if(gr1.empty()){
        sol = gr2;
        if(gr2.size()>1){
          //writeFailed(gr1, gr2,names[i-1].first,s1,names[i].first,s2,r1,r2, paired);
          //writeFailedReads(names[i-1].first,s1,names[i].first,s2,paired);
          checkFusion = true;
        }else{
          genes.push_back(gr2[0].gv);
          numassign++;
        }
      }else if(gr2.empty()){
        sol = gr1;
        if(gr1.size()>1){
          //writeFailed(gr1, gr2,names[i-1].first,s1,names[i].first,s2,r1,r2, paired);
          //writeFailedReads(names[i-1].first,s1,names[i].first,s2,paired);
          checkFusion = true;
        }else{
          genes.push_back(gr1[0].gv);
          numassign++;
        }
      }else{
        // SPLIT breakpoint is on one of the pair    read1-read2 --- read2
        // PAIR breakpoint is in the fragment   read1-gene1 ------ read2-gene2
        if(gr1.size()==2 && gr2.size()==2){
          //writeFailed(gr1, gr2,names[i-1].first,s1,names[i].first,s2,r1,r2, paired);
          //writeFailedReads(names[i-1].first,s1,names[i].first,s2,paired);
          continue;
        }else{
          if(gr1.size() != gr2.size()){
            // Possible Split
            checkFusion = true;
            //writeFailed(gr1, gr2,names[i-1].first,s1,names[i].first,s2,r1,r2, paired);
            //writeFailedReads(names[i-1].first,s1,names[i].first,s2,paired);
          }else{
            if(gr1[0].gv==gr2[0].gv){
              genes.push_back(gr1[0].gv);
              numassign++;
            }else{
              //writeFailed(gr1, gr2,names[i-1].first,s1,names[i].first,s2,r1,r2, paired);
              //writeFailedReads(names[i-1].first,s1,names[i].first,s2,paired);
              if(fusion){
                //Is Pair
                ispair = true;
              }

            }
          }
          // sol.reserve(gr1.size(),gr2.size());
          // sol.insert( sol.end(), gr1.begin(), gr1.end() );
          // sol.insert( sol.end(), gr2.begin(), gr2.end() );
          if(checkFusion && fusion){
            GeneLog g1;
            GeneLog g2;
            if(sol.empty()){
              // check with gr1 and gr2
              if(gr1.size()==1){
                if(gr2[0].gv != gr1[0].gv){
                  continue;
                }else{
                  g1 = gr2[0];
                  g2 = gr2[1];
                }
              }else{
                if(gr1[1].gv != gr2[0].gv){
                  continue;
                }else{
                  g1 = gr1[0];
                  g2 = gr1[1];
                }
              }
            }else{
              g1 = sol[0];
              g2 = sol[1];
            }
            fusionScan(g1,g2,names[i-1].first,s1,names[i].first,s2,paired,ispair);
          }
        }
      }
    }//end if
    */


  }//end for
}

/*bool ReadProcessor::validValue(valueT v,int &rep_counts,std::vector<valueT> &info){
    if(v > index.cliqs_end){
      // is alien
      rep_counts=0;
      return false;
    }
    if(v > index.cliqs_begin){
      rep_counts++;
      return false;
    }

    if (v==0){
      // is repetitive
      rep_counts++;
      return false;
    }

    return true;

}*/
std::vector<valueT> ReadProcessor::GetValues(valueT v) {
  std::vector<valueT> u;
  if (v < index.cliqs_begin) {
    u.push_back(v);
    return u;
  } else {
    return index.GetGeneClique(v).gene_vec;
  }
}

bool ReadProcessor::validValue(valueT & v, int &rep_counts) {

  if ( v > 0 && v < index.cliqs_end) {
    return true;
  }
  return false;
}

bool ReadProcessor::validValue(valueT & v) {
  if ( v > 0 && v < index.cliqs_end) {
    return true;
  }
  return false;
}



int ReadProcessor::updateWalker(std::map <valueT, GeneLog>& walker, valueT val, int pos, int &rep_counts, int s) {
  if (val == -1) {
    return 0;
  }
//std::cerr << pos << std::endl;
  auto x = walker.find(val);
  int block;
  if ( x != walker.end()) {
    x->second.tot++;
    x->second.n_rep += rep_counts;
    x->second.checkCont(pos, s);

    block = x->second.currBlock;
  } else {
    GeneLog gl = GeneLog(val, pos);
    gl.n_rep = rep_counts;
    walker.insert(pair<valueT, GeneLog>(val, gl));
    block = 1;
  }
  rep_counts = 0;
  return block;
}

// int ReadProcessor::updateWalker(std::map <valueT, GeneLog>& walker, std::vector<valueT> vals, int pos, int &rep_counts,int s){
//   int block = -1;
//   for(auto val : vals){
//     b = updateWalker(walker,val,pos,rep,1);
//     block = (block > b) ? block : b;
//   }
//   return block;
// }

valueT ReadProcessor::firstTarget(KmerCreater & kc, keyT mask, const char* s, int r_len,
                                  int& pos, int& curr) {
  // check not at the end of read

  if (pos + k > r_len ) {
    //std::cerr<< "not cool" << r_len << std::endl;
    return false;
  }

  keyT kmer;
  keyT kmer_cur;
  keyT kmer_new;
  bool mapped = false;
  //debug
#ifdef DEBUG_ASSIGN
  std::vector<valueT> vals;
  std::vector<string> val_kmers;
  char kstring[k + 1];
#endif
  valueT val;
  keyT nextNc;
  int rep = 0;
  int bsize ;
  kc.convert(s + pos, kmer);
  //kmer_new =  kc.minSelfAndRevcomp(kmer);
  val = index.othello->queryInt(kmer);
  if (validValue(val, rep)) {
#ifdef DEBUG_ASSIGN
    kc.convertString(kstring, kmer);
    //std::cerr << val << std::endl;
    vals.push_back(val);
    val_kmers.push_back(kstring);
#endif
    if (index.othelloG->queryInt(kmer) == val) {
      //std::cerr<< "pass" << std::endl;
      mapped = true;
    }


  }


  while ( !mapped && pos + k < r_len ) {
    pos ++;
    //std::cout << pos << " ";
    kmer <<= 2;
    nextNc = charToKey[*(s + pos - 1 + k) - 'A'];
    kmer = (kmer | nextNc)&mask;
    //kmer_new = kc.minSelfAndRevcomp(kmer);
    val = index.othello->queryInt(kmer);

    if (validValue(val, rep)) {
#ifdef DEBUG_ASSIGN
      kc.convertString(kstring, kmer);
      vals.push_back(val);
      val_kmers.push_back(kstring);
#endif
      if (index.othelloG->queryInt(kmer) == val) {
        //std::cerr<< "pass" << std::endl;
        mapped = true;
        break;
      }

    }

  }

  //debug
#ifdef DEBUG_ASSIGN
  for (auto v : vals) {
    std::cerr << v << " ";
  }
  for (auto v : val_kmers) {
    std::cerr << v << " ";
  }
  std::cerr << std::endl;
#endif
  if (mapped) {

    int curr_pos = pos + k;
    int score = distance(curr_pos, s);
    //std::cerr << score << std::endl;

    if (score < curr) {
      curr = score;
    } else {
      return 0;
    }
    //std::cerr << "val " << val << " " << score << std::endl;
    return val;
  } else {
    return 0;
  }
}

/*
Find the anchor gene, if all==true, will process the rest of the sequences
started at pos.
pos will be updated when return.
*/
keyT ReadProcessor::findMappedGene(KmerCreater & kc, keyT mask, const char* s, int r_len,
                                   int& pos, std::map <valueT, GeneLog> &walker, bool all) {

  if (pos + k > r_len ) {
    //std::cerr<< "not cool" << r_len << std::endl;
    return false;
  }
  bool mapped = false;
  keyT kmer;
  keyT nextNc;
  keyT kmer_new;
//debug
#ifdef DEBUG_ASSIGN
  std::vector<valueT> vals;
  std::vector<string> val_kmers;
  char kstring[k + 1];
#endif
  valueT val;

  int rep = 0;
  int bsize ;
  kc.convert(s + pos, kmer);
  kmer_new =  kc.minSelfAndRevcomp(kmer);
  val = index.othello->queryInt(kmer_new);
  if (validValue(val, rep)) {
#ifdef DEBUG_ASSIGN
    kc.convertString(kstring, kmer_new);
    vals.push_back(val);
    val_kmers.push_back(kstring);
#endif
    bsize = updateWalker(walker, val, pos, rep, 1);
    // if(!all){
    //   pos++;
    //   return true;
    // }
    if (!all) {
      pos++;
      return kmer_new;
    }
  }

//kc.updateKmers(pos,kmer_new);

  while ( !mapped && pos + k < r_len ) {
    pos ++;
    kmer <<= 2;
    nextNc = charToKey[*(s + pos - 1 + k) - 'A'];
    kmer = (kmer | nextNc)&mask;
    kmer_new = kc.minSelfAndRevcomp(kmer);
    val = index.othello->queryInt(kmer_new);

    if (validValue(val, rep)) {
#ifdef DEBUG_ASSIGN
      kc.convertString(kstring, kmer_new);
      vals.push_back(val);
      val_kmers.push_back(kstring);
#endif
      bsize = updateWalker(walker, val, pos, rep, 1);
      // if(bsize > 200){
      //   std::cerr << " bisze " << bsize << std::endl;
      //   exit (EXIT_FAILURE);
      // }

      if (!all) {
        pos++;
        return kmer_new;
      }//otherwise get all kmers instead
    }

  }
//debug
#ifdef DEBUG_ASSIGN
  for (auto v : vals) {
    std::cerr << v << " ";
  }
  for (auto v : val_kmers) {
    std::cerr << v << " ";
  }
  std::cerr << std::endl;
#endif

  return kmer_new;
}


void ReadProcessor::operator()() {

  while (true) {
    int readbatch_id = 0;
    if (true) {
      std::lock_guard<std::mutex> lock(mp.reader_lock);

      // The current empty function have BUUUUG
      // If the fq file not readable, it will be in a dead loop

      if (mp.SR.empty()) {
        // nothing to do

        return;
      } else {

        // get new sequences
        mp.SR.fetchSequences(buffer, bufsize, seqs, names, quals, umis,
                             readbatch_id, true);
      }
    }
    // for(auto seq : seqs){
    //   std::cout << seq.first << ' ' << seq.second << std::endl;
    // }

    // release the reader lock

    // process our sequences
    if (mp.opt.subset) {

      subset();
      clear();
    } else {
      //std::cerr << "mp before CELL" << std::endl;
      CELL();
      // update the results, MP acquires the lock
      mp.update(genes, repgenes, jump_count, numseqs, cell);
      clear();
    }

  }
}

ReadProcessor::ReadProcessor(ReadProcessor && o) :
  paired(o.paired),
  index(o.index),
  kc(o.kc),
  mp(o.mp),
  id(o.id),
  k(o.k),
  umi_len(o.umi_len),
  bc_len(o.bc_len),
  all(o.all),
  stride(o.stride),
  bufsize(o.bufsize),
  numseqs(o.numseqs),
  seqs(std::move(o.seqs)),
  names(std::move(o.names)),
  quals(std::move(o.quals)),
  umis(std::move(o.umis)),
  cell(std::move(o.cell)),
  genes(std::move(o.genes)),
  write_failed(o.write_failed),
  genecounts(std::move(o.genecounts)),
  repgenes(std::move(o.repgenes)),
  jump_count(std::move(o.jump_count)) {
  buffer = o.buffer;
  o.buffer = nullptr;
  o.bufsize = 0;
}

void ReadProcessor::clear() {
  numseqs = 0;
  memset(buffer, 0, bufsize);
  genes.clear();
  genecounts.clear();
  repgenes.clear();
  cell.clear();
//jump_count.clear();
  for (auto &j : jump_count) {
    j = 0;
  }
}

ReadProcessor::~ReadProcessor() {
  if (buffer != nullptr) {
    delete[] buffer;
    buffer = nullptr;
  }
}



void ReadProcessor::writeFailedReads(
  const std::string & n1,
  const std::string & s1,
  const std::string & q1,
  const std::string & n2,
  const std::string & s2,
  const std::string & q2,
  bool pairedend) {
  std::stringstream o1;
  std::stringstream o2;
  o1 << "@" << n1 << "\n";
  o1 << s1 << "\n";
  o1 << "+" << "\n";
  o1 << q1;
  if (pairedend) {
    o2 << "@" << n2 << "\n";
    o2 << s2 << "\n";
    o2 << "+" << "\n";
    o2 << q2;
  }
  mp.outputfailedReads(o1, o2);

}
void ReadProcessor::writeFailed(std::vector<valueT> &g1, std::vector<valueT> &g2, const std::string & n1, const std::string & s1,
                                const std::string & n2,
                                const std::string & s2,
//std::vector<std::pair<valueT,int>> &r1,
//std::vector<std::pair<valueT,int>> &r2,
                                bool pairedend) {
  std::stringstream o;
  o << n1 << "\t";
  o << s1 << "\t";
  for (auto g : g1) {
    o << g << "+";
  }
  o << "\t";
// for(auto r:r1){
//   o << r.first << " ";
// }
  if (pairedend) {
    o << "\t";
    o << n2 << "\t";
    o << s2 << "\t";
    for (auto g : g2) {
      o << g << "+";
    }
    o << "\t";
    // for(auto r:r2){
    //   o << r.first << " ";
    // }
  }
  mp.outputfailed(o);
}

void ReadProcessor::fusionScan(GeneLog & g1, GeneLog & g2, const std::string & n1, const std::string & s1,
                               const std::string & n2,
                               const std::string & s2,
                               bool pairedend, bool ispair) {
  std::stringstream o;
  if (ispair) {
    o << "PAIR\t";
    o << n1 << "\t";
    o << s1 << "\t";
    o << n2 << "\t";
    o << s2 << "\t";
    o << g1.gv << "," << g2.gv;
    mp.outputFusion(o);
  } else {
    // not consider rep kmers, need to check
    int dist = g2.first_p - g1.last_p - 1;
    if (dist > k - 2 && dist < k + 2 ) {
      o << "SPLIT\t";
      o << n1 << "\t";
      o << s1 << "\t";
      if (pairedend) {
        o << n1 << "\t";
        o << s1 << "\t";
      } else {
        o << "NA\tNA\t";
      }
      o << g1.gv << "," << g2.gv;
      mp.outputFusion(o);
    }
  }

}

void ReadProcessor::printKmer(std::vector<std::pair<valueT, int>> & r,
                              const char* s, int l) {
  for (int i = 0; i < r.size(); i++) {
    std::cout << r[i].first << '\t' << r[i].second ;
    std::cout << "\t" ;
    for (int j = 0; j < i; j++) {
      printf(" ");
    }
    printf("%.*s", 21, s + i);
    std::cout << std::endl;
  }
}


//@M

void MasterProcessor::subsetReads() {
  std::cerr << "Start thread" << endl;
  std::vector<std::thread> workers;
  for (int i = 0; i < opt.threads; i++) {
    std::cerr << "job: " << i << std::endl;
    workers.emplace_back(std::thread(ReadProcessor(index, opt, kc, *this)));
  }
  std::cerr << "Start thread join" << endl;
// let the workers do their thing
  for (int i = 0; i < opt.threads; i++) {
    std::cerr << "join job: " << i << std::endl;
    workers[i].join(); //wait for them to finish
  }
  std::cerr << "Start thread join done" << endl;
}



void MasterProcessor::outputFusion(const std::stringstream & o) {
  std::string os = o.str();
  if (!os.empty()) {
    std::lock_guard<std::mutex> lock(this->writer_lock);
    ofusion << os << "\n";
  }
}

void MasterProcessor::outputfailed(const std::stringstream & o) {
  std::string os = o.str();
  if (!os.empty()) {
    std::lock_guard<std::mutex> lock(this->writer_lock);
    failed << os << "\n";
  }
}

void MasterProcessor::outputfailedReads(const std::stringstream & o1,
                                        const std::stringstream & o2) {
  std::string os1 = o1.str();
  std::string os2 = o2.str();
  if (!os1.empty()) {
    std::lock_guard<std::mutex> lock(this->writer_lock);
    failed_r1 << os1 << "\n";
  }
  if (!os2.empty()) {
    std::lock_guard<std::mutex> lock(this->writer_lock);
    failed_r2 << os2 << "\n";
  }
}




















/* NOT IN USE */
std::vector<GeneLog> ReadProcessor::classify(std::vector<std::pair<valueT, int>> &v, int r_len, int stride, bool fusion) {
  valueT gene1 = NULL;
  valueT gene2 = NULL;
  valueT tmp;

  int def_counts = 0;
  int rep_counts = 0;
  std::map <valueT, GeneLog> walker;
//std::cerr << v.size() << std::endl;
  for (auto kv : v) {
    tmp = kv.first;
    // Currently not process informative
    if (tmp > index.cliqs_end) {
      rep_counts = 0;
      continue;
    }
    if (tmp > index.cliqs_begin) {
      rep_counts++;
      continue;
    }

    if (tmp == 0) {
      // is repetitive
      rep_counts++;
      continue;
    }

    if (tmp <= index.num_genes) {
      def_counts++;
      // is definitive
      auto x = walker.find(tmp);
      if ( x != walker.end()) {
        x->second.tot++;
        x->second.n_rep += rep_counts;
        x->second.checkCont(kv.second, stride);
        rep_counts = 0;
      } else {
        GeneLog gl = GeneLog(tmp, kv.second);
        gl.n_rep = rep_counts;
        walker.insert(pair<valueT, GeneLog>(tmp, gl));
        rep_counts = 0;
      }
    }
    /* implementation for informative kmer */
    // }else{
    //  // is informative
    //  // auto cliq = cliqueidx.find(tmp);

    // }
  }
  if (walker.empty()) {
    //std::cerr <<"kong" << std::endl;
    return {};
  }

  std::vector<GeneLog> top_list;
  for ( auto &m : walker ) {
    // printf("last_p %d, first_p %d total %d n_rep %d\n",m.second.last_p,
    //  m.second.first_p,m.second.tot,m.second.n_rep);
    //std::cerr << "block " << m.second.maxBlock
    //<< "mismatch " << m.second.estMis(1,2,k) << std::endl;

    // check:
    //  1. total kmer + rep kmer, currently only include before or inbetween
    //  2. check continuous block size
    //  3. possible mismatch
    // std::cerr << m.second.gv <<" "<<((m.second.tot + m.second.n_rep) > ( r_len/3 - k)) <<" "<< (m.second.maxBlock > 5) << " "<< m.second.estMis(1,2,k) << std::endl;
    if (((m.second.tot + m.second.n_rep) > ( r_len / 3 - k)) && (m.second.maxBlock > 5) && m.second.estMis(1, 2, k)) {
      //will filter mismatch over 1
      top_list.push_back(m.second);
    }

  }
  int n_genes = top_list.size();
  if (n_genes == 0 || n_genes > 2) {
    //std::cerr << "empty or to many" << std::endl;
    return {};
  }
  if (n_genes == 1) {
    return top_list;
  }
//std::sort(top_list.begin(),top_list.end());
  return top_list;

  /*
  if(n_genes == 1){
    for( auto &m:walker){
      top_list.push_back({m.first,m.second});
    }
    return top_list;
  }
  n_genes = 2;
  std::vector<std::pair<valueT, GeneLog>> top_list(n_genes);
  std::partial_sort_copy(walker.begin(),
                         walker.end(),
                         top_list.begin(),
                         top_list.end(),
                         [](std::pair<const valueT, GeneLog> const& l,
                            std::pair<const valueT, GeneLog> const& r)
                         {
                             return l.second.tot > r.second.tot;
                         });

  for(int i = 0; i < n_genes; i++){
    printf("%d: count = %d, value = %d, mb = %d, fpos = %d, lpos = %d\n",
      i,top_list[i].second.tot,
      top_list[i].first,
      top_list[i].second.maxBlock,
      top_list[i].second.first_p,
      top_list[i].second.last_p);
  }

  return top_list;*/
// How to estimate mismatches

// if( !fusion ){
//  // Only one candidate, use top 1
//  // max_mismatch = 2, will have at most 42 kmers missing?
//  // will not consider bases beyong the last_p?
//  // xxxx-mmmmmmmmm-xx-mmmmmmm-xxxxxxxx
//  if (top_list[0].second.estMis(1,1)){
//    std::cerr << "mismatch > 2" << std::endl;
//  }else{
//    std::cerr << "increase count " << top_list[0].first << std::endl;
//    o[top_list[1].first-1].second++;
//  }
// }else{
//  std::cerr<<"Fusion" << std::endl;
//  if(n_genes==2){
//    if(top_list[0].second.estMis(1,1)&&top_list[1].second.estMis(1,1)){
//      // check gap

//      int gap = 0;
//      bool topFirst = false;
//      if(top_list[0].second.last_p < top_list[1].second.first_p){
//        // -- 0 -- 1 --
//        gap = top_list[1].second.first_p - top_list[0].second.last_p - 1;
//        topFirst = true;
//      }else{
//        // -- 1 -- 0 --

//        gap = top_list[0].second.first_p - top_list[1].second.last_p - 1;
//      }
//      std::cerr<<"gap "<< gap << std::endl;

//    }else{
//      std::cerr<<"Toomany" << std::endl;
//    }
//  }else{
//    std::cerr<<"what" << std::endl;
//  }

// }
// if( !fusion ){
//  if(top_list[0].second.tot/def_counts >= 0.5 && top_list[0].second.maxBlock/top_list[0].second.tot*1.0 >=0.8){
//    auto oi = o.find(top_list[i].first)
//    if(oi != o.end()){
//      o->second++;
//    }else{
//      o.insert(pair<valueT,int>(top_list[i].first,1));
//    }
//  }
// }

}

void ReadProcessor::processBuffer() {
  /*
    1. iterate through reads, extract kmers and assign kmer to gene
    2. assign read to gene
    counts: vector of gv key
  */
  int l1, l2;
  std::vector<std::pair<valueT, int>> r1;
  std::vector<std::pair<valueT, int>> r2;
  const char* s1 = 0;
  const char* s2 = 0;
  for (int i = 0; i < seqs.size(); i++) {
    s1 = seqs[i].first;
    l1 = seqs[i].second;
#ifdef PRINT_DEBUG
    std::cerr << s1 << std::endl;
#endif
    if (paired) {
      i++;
      s2 = seqs[i].first;
      l2 = seqs[i].second;
    }

    numseqs++;
    r1.clear();
    r2.clear();

    // process read
    index.match(s1, l1, r1);
    if (paired) {
      index.match(s2, l2, r2);
    }
#ifdef PRINT_DEBUG
    printKmer(r1, s1, l1);
    printKmer(r2, s2, l2);
#endif
    std::vector<GeneLog> gr1;
    std::vector<GeneLog> gr2;
    gr1.clear();
    gr2.clear();

    gr1 = classify(r1, l1, 1, true);
    if (paired) {
      gr2 = classify(r2, l2, 1, true);
    }

    /* first try to assign reads,
       if not assigned, check whether a fusion event
    */
    if (gr1.empty() && gr2.empty()) {
      // fail
      //std::cerr << names[i-1].first <<"\n" << s1 << std::endl;
      //writeFailed(gr1, gr2,names[i-1].first,s1,names[i].first,s2,r1,r2, paired);
      //writeFailedReads(names[i-1].first,s1,names[i].first,s2,paired);
      continue;
    }
    std::vector<GeneLog> sol;
    sol.clear();
    bool checkFusion = false;
    bool ispair = false;
    if (gr1.empty()) {
      sol = gr2;
      if (gr2.size() > 1) {
        //writeFailed(gr1, gr2,names[i-1].first,s1,names[i].first,s2,r1,r2, paired);
        //writeFailedReads(names[i-1].first,s1,names[i].first,s2,paired);
        checkFusion = true;
      } else {
        genes.push_back(gr2[0].gv);
      }
    } else if (gr2.empty()) {
      sol = gr1;
      if (gr1.size() > 1) {
        //writeFailed(gr1, gr2,names[i-1].first,s1,names[i].first,s2,r1,r2, paired);
        //writeFailedReads(names[i-1].first,s1,names[i].first,s2,paired);
        checkFusion = true;
      } else {
        genes.push_back(gr1[0].gv);
      }
    } else {
      // SPLIT breakpoint is on one of the pair    read1-read2 --- read2
      // PAIR breakpoint is in the fragment   read1-gene1 ------ read2-gene2
      if (gr1.size() == 2 && gr2.size() == 2) {
        //writeFailed(gr1, gr2,names[i-1].first,s1,names[i].first,s2,r1,r2, paired);
        //writeFailedReads(names[i-1].first,s1,names[i].first,s2,paired);
        continue;
      } else {

        if (gr1.size() != gr2.size()) {
          // Possible Split
          checkFusion = true;
          //writeFailed(gr1, gr2,names[i-1].first,s1,names[i].first,s2,r1,r2, paired);
          //writeFailedReads(names[i-1].first,s1,names[i].first,s2,paired);
        } else {
          if (gr1[0].gv == gr2[0].gv) {
            genes.push_back(gr1[0].gv);
          } else {
            //writeFailed(gr1, gr2,names[i-1].first,s1,names[i].first,s2,r1,r2, paired);
            //writeFailedReads(names[i-1].first,s1,names[i].first,s2,paired);
            if (fusion) {
              //Is Pair
              ispair = true;
            }

          }
        }
        // sol.reserve(gr1.size(),gr2.size());
        // sol.insert( sol.end(), gr1.begin(), gr1.end() );
        // sol.insert( sol.end(), gr2.begin(), gr2.end() );
        if (checkFusion && fusion) {
          GeneLog g1;
          GeneLog g2;
          if (sol.empty()) {
            // check with gr1 and gr2
            if (gr1.size() == 1) {
              if (gr2[0].gv != gr1[0].gv) {
                continue;
              } else {
                g1 = gr2[0];
                g2 = gr2[1];
              }
            } else {
              if (gr1[1].gv != gr2[0].gv) {
                continue;
              } else {
                g1 = gr1[0];
                g2 = gr1[1];
              }
            }
          } else {
            g1 = sol[0];
            g2 = sol[1];
          }
          fusionScan(g1, g2, names[i - 1].first, s1, names[i].first, s2, paired, ispair);
        }
      }
    }
  }
}
