#pragma once
#include <zlib.h>
#include "kseq.h"
#include <string>
#include <vector>
#include <unordered_map>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <chrono>
#include <thread>
#include <mutex>
#include <atomic>
#include <condition_variable>

#include "common.h"
#include "OthelloIndex.h"
#include "SequenceReader.h"
#include "GeneCounts.h"
#include "KmerCreater.h"

#ifndef KSEQ_INIT_READY
#define KSEQ_INIT_READY
KSEQ_INIT(gzFile, gzread)
#endif


//void Append(std::vector<T>& a, const std::vector<T>& b);
class MasterProcessor;
class CellProcessor {
  CellProcessor(const OthelloIndex &index, const Opts& opt,KmerCreater& kc,MasterProcessor& mp);
  CellProcessor(CellProcessor && o);
  ~CellProcessor();
  char *buffer;
  size_t bufsize;
  bool all;
  MasterProcessor& mp;
  KmerCreater &kc;
  keyT last_kmer = -1;
  int numseqs;
  int numassign;
  std::vector <int> jump_count{vector<int>(100,0)};
  int id;
  int k;
  int stride;
  bool write_failed;
  const OthelloIndex &index;
  std::vector<std::pair<const char*, int>> seqs;
  std::vector<std::pair<const char*, int>> names;
  std::vector<std::pair<const char*, int>> quals;
};

class ReadProcessor {
public:
  ReadProcessor(const OthelloIndex &index, const Opts& opt,KmerCreater& kc,MasterProcessor& mp);
  ReadProcessor(ReadProcessor && o);
  ~ReadProcessor();
  char *buffer;
  size_t bufsize;
  bool paired;
  bool fusion;
  bool all;
  MasterProcessor& mp;
  KmerCreater &kc;
  keyT last_kmer = -1;
  int numseqs;
  int numassign;
  std::vector <int> jump_count{vector<int>(100,0)};
  int id;
  int k;
  int stride;
  bool write_failed;
  const OthelloIndex &index;
  int umi_len;
  int bc_len;
  //const char prime3[8] = "GTTTTAG\0";
  //const char prime5[8] = "AACACCG\0";
  //Opts &opt;
  // store sequences
  std::vector<std::pair<const char*, int>> seqs;
  std::vector<std::pair<const char*, int>> names;
  std::vector<std::pair<const char*, int>> quals;
  std::vector<std::string> umis;
  // gene counts
  std::vector<valueT> genes;
  //std::vector<std::set<keyT>> cell;

  // {cell_type , umi}
  std::vector<std::pair<Cell,keyT>> cell;
  
  std::vector<std::vector<valueT>> repgenes;
  std::vector<double> genecounts;
  void fusionScan(GeneLog &g1, GeneLog &g2,const std::string &n1, const std::string &s1, const std::string &n2,const std::string &s2,bool pairedend, bool ispair);
  std::vector<GeneLog> classify (std::vector<std::pair<valueT,int>> &v, int r_len, int stride, bool fusion);
  valueT findAnchor(const char* s, keyT& lk, int& pos, int len);
  valueT findAnchor(const char* s, keyT& lk, int& pos, int len, int &rep_counts);
  std::vector<valueT> ChewSeq(const char* s,int r_len);
  std::vector<valueT> ChewSeqMM(const char* s,int r_len);
  void operator()();
  void processBuffer();
  void CELL();
  void printKmer(std::vector<std::pair<valueT,int>> & r, 
  const char* s, int l);
  void writeFailed(std::vector<valueT> &g1, std::vector<valueT> &g2,
    const std::string &n1, const std::string &s1,
  const std::string &n2,
  const std::string &s2,
  //std::vector<std::pair<valueT,int>> &r1,
  //std::vector<std::pair<valueT,int>> &r2,
  bool pairedend);
  void writeFailedReads(const std::string &n1, const std::string &s1, const std::string &q1, const std::string &n2, const std::string &s2, const std::string &q2, bool pairedend);
  std::vector<valueT> Teeth(const char* s,int r_len, int stride);
  std::vector<valueT> Teeth(const char* s,int r_len);
  int updateWalker(std::map <valueT, GeneLog>& walker, valueT val, int pos, int &rep_counts,int stride);
  bool validValue(valueT &v,int &rep_counts);
  bool validValue(valueT &v);
  keyT findMappedGene(KmerCreater& kc, keyT mask, const char* s, int r_len,
                  int& pos, std::map <valueT, GeneLog> &walker, bool all);
  valueT firstTarget(KmerCreater& kc, keyT mask, const char* s, int r_len,
                  int& pos, int& curr);
  int distance(int pos,const char* s);
  void subset();
  std::vector<valueT> GetValues(valueT v);
  void Monster();
  void clear();
};


class MasterProcessor {
public:
  MasterProcessor (OthelloIndex &index, KmerCreater &KC, GeneCounter &GC, 
    GeneCounter &GCre,Opts& opt)
    : index(index),kc(KC), gc(GC), gcrep(GCre),opt(opt), SR(opt), numreads(0)
    ,nummapped(0),bufsize(1ULL<<23) {
      if(opt.fusion){
        ofusion.open("fusion.tsv");
      }
      if(opt.output_failed){
        failed.open("failed.tsv");
        failed_r1.open("failed_r1.fastq");
        if(!opt.single_end){
          failed_r2.open("failed_r2.fastq");
        }
      }


    };

  ~MasterProcessor(){};

  std::mutex reader_lock;
  std::mutex writer_lock;


  SequenceReader SR;
  OthelloIndex &index;
  KmerCreater &kc;
  GeneCounter &gc;
  GeneCounter &gcrep;

  //cells is cell_type, {umi,counts}
  std::map<Cell,std::map<keyT,uint32_t>> cells;
  std::vector<std::vector<valueT>> repReads;
  Opts& opt;
  int numreads;
  int nummapped;
  std::vector <int> JCount{vector<int>(100,0)};
  
  size_t bufsize;
  std::ofstream ofusion;
  std::ofstream failed;
  std::ofstream failed_r1;
  std::ofstream failed_r2;
  void outputFusion(const std::stringstream &o);
  void outputfailed(const std::stringstream &o);
  void outputfailedReads(const std::stringstream &o1,
  const std::stringstream &o2);
  void processReads();
  void subsetReads();
  // cell_count is {cb,[g1,g2,..]}
  std::unordered_map<keyT,GeneCounter> cell_count;
  void update(const std::vector<valueT>& g, 
    const std::vector<std::vector<valueT>>&gv, 
    std::vector<int> &c,int numseqs,
    std::vector<std::pair<Cell,keyT>>& cell);
};
