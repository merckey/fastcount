#pragma once
#include "common.h"
#include <memory>
#include <fstream>
#include <sstream>
#include <iostream>
#include <zlib.h>
#include "kseq.h"
#include <string>
#include <vector>
#include <unordered_map>
#include <algorithm>
#include <thread>
#include <mutex>
#include <atomic>
#include <condition_variable>
#ifndef KSEQ_INIT_READY
#define KSEQ_INIT_READY
KSEQ_INIT(gzFile, gzread)
#endif

class SequenceReader {
public:

  SequenceReader(const Opts& opt) :
  fp1(0),fp2(0),seq1(0),seq2(0),
  l1(0),l2(0),nl1(0),nl2(0),
  paired(!opt.single_end), files(opt.seq_files),
  f_umi(new std::ifstream{}),
  current_file(0), state(false), readbatch_id(-1) {}
  SequenceReader() :
  fp1(0),fp2(0),seq1(0),seq2(0),
  l1(0),l2(0),nl1(0),nl2(0),
  paired(false), 
  f_umi(new std::ifstream{}),
  current_file(0), state(false), readbatch_id(-1) {}

  // https://stackoverflow.com/questions/5481539/what-does-t-double-ampersand-mean-in-c11
  // rvalue reference
  // https://stackoverflow.com/questions/3106110/what-are-move-semantics
  SequenceReader(SequenceReader&& o);
  
  bool empty();
  void reset();
  ~SequenceReader();
  // note: read seqeunce to <reads,len> vector
  bool fetchSequences(char *buf, const int limit, std::vector<std::pair<const char*, int>>& seqs,
                      std::vector<std::pair<const char*, int>>& names,
                      std::vector<std::pair<const char*, int>>& quals,
                      std::vector<std::string>& umis, int &readbatch_id,
                      bool full=false);

public:
  gzFile fp1 = 0, fp2 = 0;
  kseq_t *seq1 = 0, *seq2 = 0;
  int l1,l2,nl1,nl2;
  bool paired;
  std::vector<std::string> files;
  std::vector<std::string> umi_files;
  std::unique_ptr<std::ifstream> f_umi;
  int current_file;
  bool state; // is the file open
  int readbatch_id = -1;
};