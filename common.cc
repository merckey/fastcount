#include "common.h"
std::vector<std::string> splitStr(std::string str, char delimiter) {
	std::vector<std::string> internal;
	std::stringstream ss(str); // Turn the string into a stream.
	std::string tok;

	while(getline(ss, tok, delimiter)) {
		internal.push_back(tok);
	}

	return internal;
}

// int HammingDistKeyT(keyT x, keyT y) {
//   keyT i = x ^ y;
//   i = (i & 0x5555555555555555) | ((i & 0xAAAAAAAAAAAAAAAA) >> 1);
//   i = i - ((i >> 1) & 0x5555555555555555);
//   i = (i & 0x3333333333333333) +
//       ((i >> 2) & 0x3333333333333333);
//   i = ((i + (i >> 4)) & 0x0F0F0F0F0F0F0F0F);
//   return (i * (0x0101010101010101)) >> 56;
// }

bool HWgt1(keyT x, keyT y){
  int count;
  keyT i = x ^ y;
  i = (i & 0x5555555555555555) | ((i & 0xAAAAAAAAAAAAAAAA) >> 1);
  for (count=0; i; count++) {
    if(count > 1){
      return 1; 
    }
    i &= i - 1;
  }
  return count > 1;
}

// int NumberOfSetBits64(keyT x, keyT y)
// {
//   keyT i = x ^ y;
//   i = i - ((i >> 1) & 0x5555555555555555);
//   i = (i & 0x3333333333333333) +
//       ((i >> 2) & 0x3333333333333333);
//   i = ((i + (i >> 4)) & 0x0F0F0F0F0F0F0F0F);
//   return (i * (0x0101010101010101)) >> 56;
// }
