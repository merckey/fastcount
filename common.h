#pragma once
#include <string>
#include <vector>
#include <sstream>

typedef uint64_t keyT;
typedef uint32_t valueT;
// jp
std::vector<std::string> splitStr(std::string str, char delimiter);
bool HWgt1(keyT x, keyT y);
/* initialize struct with ctor */
struct Opts {
	int threads;
	int k;
	bool single_end;
	bool batch_mode;
	bool fusion;
	int stride;
	bool output_failed;
	bool all_kmer;
	bool tx2gene_file;
	bool parse_gencode;
	bool make_unique;
	bool sgRNA;
	bool scRNA;
	bool subset;
	int umi_len;
	int bc_len;
	std::vector<std::string> seq_files;
	
	std::string prefix;
	std::string index_file;
	std::string txlist;
	std::string geneids_file;
	std::string cliques_file;
	std::string index2_file;
	std::string sgrna_file;
	std::string kmer_file;
	std::string info_kmer_file;
	std::string rep_kmer_file;
	std::string barcode;
	std::vector<std::string> transfasta;

	// initialization
	Opts():
	threads(5),
	k(21),
	single_end(true),
	fusion(false),
	output_failed(false),
	tx2gene_file(false),
	parse_gencode(true),
	batch_mode(false),
	sgRNA(false),
	scRNA(false),
	subset(false)
	{}
	void make_file(std::string pf, bool isSgRNA){
		//sgRNA = isSgRNA;
		prefix = pf;
		index_file = prefix+".bin";
		kmer_file = prefix+".kmer";
		geneids_file = prefix+".geneid";
		
		if(sgRNA){
			sgrna_file = prefix+"_gRNA.txt";
			index2_file = prefix+"_guide.bin";
		}else{
			info_kmer_file = prefix+".infokmer";
			rep_kmer_file = prefix+".repkmer";
			cliques_file = prefix+"_cliques";
		}
	}

};

