#pragma once
#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <errno.h>
#include <queue>
#include <algorithm>
#include <cstring>
#include <bitset>
#include <ctime>
#include <chrono>
#include <zlib.h>

//! convert a 64-bit Integer to human-readable format in K/M/G. e.g, 102400 is converted to "100K".
std::string human(uint64_t word); 

//! split a c-style string with delimineter chara.
std::vector<std::string> split(const char * str, char deli) ;

void printcurrtime(); 
double getrate(uint32_t ma, uint32_t mb, uint32_t da, uint32_t db);