#include <iostream>
#include <random>
#include <sstream>
#include <vector>
#include <string>
#include <cstdio>
#include <time.h>
#include <getopt.h>
#include <bitset>
#include "SequenceReader.h"
#include "OthelloIndex.h"
#include "ProcessSeq.h"
#include "KmerCreater.h"
#include "GeneCounts.h"
using namespace std;



//startup r.fq r2.fq n_thread k stride write_fail use_all_kmer(overide stride) fusion

void ParseOptionsIndex(int argc, char **argv, Opts& opt) {
  int verbose_flag = 0;
  int make_unique_flag = 0;
  const char *opt_string = "i:k:t:";
  int gRNA_flag = 0;
  static struct option long_options[] = {
    // long args
    {"verbose", no_argument, &verbose_flag, 1},
    {"make-unique", no_argument, &make_unique_flag, 1},
    {"sgrna", no_argument, &gRNA_flag, 1},
    // short args
    {"index", required_argument, 0, 'i'},
    {"kmer-size", required_argument, 0, 'k'},
    {"transcripts",required_argument,0,'t'},
    {0, 0, 0, 0}
  };
  int c;
  int option_index = 0;
  while (true) {
    c = getopt_long(argc, argv, opt_string, long_options, &option_index);

    if (c == -1) {
      break;
    }

    switch (c) {
    case 0:
      break;
    case 'i': {
      opt.prefix = optarg;
      break;
    }
    case 'k': {
      stringstream(optarg) >> opt.k;
      break;
    }
    case 't': {
      opt.txlist = optarg;
      break;
    }
    default: break;
    }
  }

  if (make_unique_flag) {
    opt.make_unique = true;
  }

  if (gRNA_flag) {
    opt.sgRNA = true;
  }
  opt.make_file(opt.prefix, opt.sgRNA);
  for (int i = optind; i < argc; i++) {
    opt.transfasta.push_back(argv[i]);
  }

}
void ParseOptionsCount(int argc, char **argv, Opts& opt) {
  int verbose_flag = 0;
  int make_unique_flag = 0;
  int detect_fusion_flag = 0;
  int use_allkmer_flag = 0;
  int pairend_flag = 0;
  int output_failed_flag = 0;
  int gRNA_flag = 0;
  int scRNA_flag = 0;
  int subset_flag = 0;
  const char *opt_string = "i:k:s:t:w:u:b:";
  static struct option long_options[] = {
    // long args
    {"verbose", no_argument, &verbose_flag, 1},
    {"make-unique", no_argument, &make_unique_flag, 1},
    {"all-kmer", no_argument, &use_allkmer_flag, 1},
    {"fusion", no_argument, &detect_fusion_flag, 1},
    {"failed", no_argument, &output_failed_flag, 1},
    {"paired", no_argument, &pairend_flag, 1},
    {"gRNA", no_argument, &gRNA_flag, 1},
    {"scRNA", no_argument, &scRNA_flag, 1},
    {"subset", no_argument, &subset_flag, 1},
    // short args
    {"index-prefix", required_argument, 0, 'i'},
    {"kmer-size", required_argument, 0, 'k'},
    {"stride", required_argument, 0, 's'},
    // {"geneids_file", required_argument, 0, 'g'},
    // {"kmer-cliques-file", required_argument, 0, 'c'},
    {"threads", required_argument, 0, 't'},
    {"whitelist", required_argument, 0, 'w'},
    {"umi_len",required_argument,0,'u'},
    {"bc_len",required_argument,0,'b'},
    {0, 0, 0, 0}
  };
  int c;
  int option_index = 0;
  while (true) {
    c = getopt_long(argc, argv, opt_string, long_options, &option_index);

    if (c == -1) {
      break;
    }

    switch (c) {
    case 0:
      break;
    case 'i': {
      opt.prefix = optarg;
      break;
    }
    case 'k': {
      stringstream(optarg) >> opt.k;
      break;
    }
    // case 'g':{
    //   opt.geneids_file = optarg;
    //   break;
    // }
    // case 'c':{
    //   opt.cliques_file = optarg;
    //   break;
    // }
    case 's': {
      stringstream(optarg) >> opt.stride;
      break;
    }
    case 't': {
      stringstream(optarg) >> opt.threads;
      break;
    }
    case 'w': {
      stringstream(optarg) >> opt.barcode;
      break;
    }
    case 'u': {
      stringstream(optarg) >> opt.umi_len;
      break;
    }
    case 'b': {
      stringstream(optarg) >> opt.bc_len;
      break;
    }

    default: break;
    }
  }

  if (make_unique_flag) {
    opt.make_unique = true;
  }
  if (use_allkmer_flag || opt.stride == 1) {
    opt.all_kmer = true;
  }
  if (detect_fusion_flag) {
    opt.fusion = true;
  }
  if (pairend_flag) {
    opt.single_end = false;
  }
  if (output_failed_flag) {
    std::cerr << "write failed" << std::endl;
    opt.output_failed = true;
  }
  if (gRNA_flag) {
    std::cerr << "run for sgRNA" << std::endl;
    opt.sgRNA = true;
  }
  if (scRNA_flag) {
    std::cerr << "run for scRNA" << std::endl;
    opt.scRNA = true;
  }
  if (subset_flag) {
    std::cerr << "run for scRNA" << std::endl;
    opt.subset = true;
  }



  opt.make_file(opt.prefix, opt.sgRNA);
  for (int i = optind; i < argc; i++) {
    opt.seq_files.push_back(argv[i]);
  }
}



int main(int argc, char **argv) {
  std::cout.sync_with_stdio(false);
  setvbuf(stdout, NULL, _IOFBF, 1048576);


  if (argc < 2) {
    exit(1);
  } else {
    Opts opt;
    string cmd(argv[1]);
    if (cmd == "version") {
      cerr << "v1" << endl;
    } else if (cmd == "index") {
      cerr << endl;
      if (argc == 2) {
        return 0;
      }
      ParseOptionsIndex(argc - 1, argv + 1, opt);
      // create an index
      OthelloIndex othIndex(opt);
      if (opt.sgRNA) {
        std::cerr << "Build gRNA index..." << std::endl;
        othIndex.BuildSgRNA(opt);
      } else {
        bool use_existing = false;
        if (use_existing) {
          // build index from existing kmer file
          othIndex.BuildIndex(opt);
        } else {
          othIndex.CountKmer(opt);
          othIndex.BuildIndex(opt);
        }
      }

    } else if (cmd == "count") {
      if (argc == 2) {
        cerr << "run count" << endl;
      }
      ParseOptionsCount(argc - 1, argv + 1, opt);
      KmerCreater kc(opt.stride, opt.k, 0);
      OthelloIndex othIndex(opt);
      if (opt.scRNA) {
        std::cerr << "build barcode index" << std::endl;
        othIndex.BarcodeIndex(16, opt);
      }
      othIndex.LoadIndex(opt);
      std::cerr << "Total gene in index: " << othIndex.num_genes << std::endl;
      std::cerr << "Clique start: " << othIndex.cliqs_begin << std::endl;
      std::cerr << "Clique end: " << othIndex.cliqs_end << std::endl;
      std::cerr << "K=" << opt.k << " UMI="<<opt.umi_len << " BC=" <<opt.bc_len<<std::endl;
      GeneCounter gc(othIndex, opt);
      GeneCounter gcr(othIndex, opt);
      MasterProcessor MP(othIndex, kc, gc, gcr, opt);
      //ReadProcessor RP(othIndex, opts, MP);
      if (opt.subset) {
        MP.subsetReads();
      } else {
        MP.processReads();
      }

    } else if (cmd == "test1") {
      KmerCreater kct(1, 21, 1111);
      int count = 0;
      //A,0 T,3 G,2 C,1
      char *read1 = "AAGTCGAATAAAAAATAATCG";
      char *read2 = "AAAAAAAAAAAAAAAAAACGA";
      char *read3 = "AAAAAAAAAAAAAAAAAACGT";
      int len = 21;
      keyT first;
      keyT second;
      keyT third;
      kct.convert(read1, first);
      kct.convert(read2, second);
      kct.convert(read3, third);
      std::bitset<64> b1(first);
      std::bitset<64> b2(second);
      std::bitset<64> b3(third);
      std::cerr << b1 << std::endl;
      std::cerr << b2 << std::endl;
      std::cerr << b3 << std::endl;
      std::cerr << first << " " << second << std::endl;
      // std::cerr << "1 vs 2 " << HammingDistKeyT(first, second) << std::endl;
      // std::cerr << "3 vs 2 " << HammingDistKeyT(second, third) << std::endl;
      std::cerr << "3 vs 2 " << HWgt1(third, second) << std::endl;
      std::cerr << "1 vs 1 " << HWgt1(first, first) << std::endl;






    } else if (cmd == "test") {
       KmerCreater kct(1, 10, 1111);
       char *read1 = "TCAGACCTTA";
       char *read2 = "TAAAAAAAAG";
       keyT before;
       keyT after;
       kct.convert(read1,before);
       kct.convert(read2,after);
       std::cerr << read1 << "<-->" << std::bitset<20>(before) << std::endl;
       std::cerr << read2 << "<-->" << std::bitset<20>(after) << std::endl;
       keyT mask = 0x000003ff;
       keyT A = 0;
       std::cerr << "mask" << "<-->" << std::bitset<20>(~(3<<18)) << std::endl;
       std::cerr << read1 << "<-->" << std::bitset<20>(before & (~(3<<18))) << std::endl;
       std::cerr << read1 << "<-->" << std::bitset<20>(2<<18) << std::endl;
       std::cerr << read1 << "<-->" << std::bitset<20>((before & (~(3<<18))) | (2<<18)) << std::endl;
       std::vector<int>ACTG = {0,1,2,3};
       keyT oo = 3;
       char tmp[10];
       for(auto i:ACTG){
        for(int pos=0; pos <10 ; pos++){
          keyT char_mask = oo <<( pos*2);
          keyT change_char = (before & (~char_mask)) | (i << pos*2);
          kct.convertString(tmp,change_char);
          std::cerr << std::bitset<20>(change_char) << "    " <<tmp << std::endl;

        }
        
       }
      // /* Test kmer creater */
      // int jump = 21;
      // KmerCreater kct(jump, 21, 1111);
      // int count = 0;
      // char *read1 = "TTGCTGATCTGAGCACAGAATCCGAAGTCCGAGAGCTTCACCCTGCCATCGAGGGTCAGCAGGATGGAGTCACTC";
      // //char *read1 = "TACAGATGCCCGCCACCACGCCGGGCTGATTTTTGTATTTCC";
      // int len = 75;
      // keyT first;
      // kct.convert(read1, first);
      // std::cerr << first << "<-->" << first << std::endl;
      // keyT kt = first;
      // keyT kc = first;
      // keyT last;
      // int pos_t = 0;
      // int pos_c = 0;
      // char s_t[21];
      // char s_c[21];
      // std::cerr << "TEST: whole kmer vs concat kmer accuracy!" << std::endl;
      // while (pos_t + 21 + jump <= len) {
      //   count ++;
      //   last = kct.NextKey(kt, read1, pos_t);
      //   pos_c  = pos_c + jump;
      //   kct.convert(read1 + pos_c, kc);
      //   if (kt == kc) {
      //     std::cerr << kc << "<-->" << kt << std::endl;
      //   }
      //   // kcc.convertString(s_c,kc);
      //   // kct.convertString(s_t,kt);
      //   // std::cerr << s_c << " " << pos_c << " <-- " << s_t << " --> " << kt << std::endl;

      // }

      // int k = 21;
      //  sheet 1
      // std::cerr << "TEST: whole kmer vs concat kmer speed!" << std::endl;
      // for(int stride = 1; stride < 22; stride+=2){
      //   auto start = std::chrono::high_resolution_clock::now();

      //   for(int i = 0; i < 1000000; i++){
      //     KmerCreater kk(stride, k, 1111);
      //     keyT first;
      //     kk.convert(read1, first);
      //     int pos = 0;
      //     while(pos + k + stride <= len){
      //       first = kk.NextKey(first,read1,pos);
      //     }
      //   }
      //   auto end = std::chrono::high_resolution_clock::now();
      //   std::chrono::duration<double> elapsed = end - start;
      //   std::cerr << "concat," << stride <<","<< elapsed.count() << std::endl;
      // }
      // for(int stride = 1; stride < 22; stride+=2){
      //   auto start = std::chrono::high_resolution_clock::now();

      //   for(int i = 0; i < 1000000; i++){
      //     KmerCreater kk(stride, k, 1111);
      //     keyT first;
      //     kk.convert(read1, first);
      //     int pos = 0;
      //     while(pos + k + stride <= len){
      //       pos = pos + stride;
      //       kct.convert(read1+pos,first);
      //     }
      //   }
      //   auto end = std::chrono::high_resolution_clock::now();
      //   std::chrono::duration<double> elapsed = end - start;
      //   std::cerr << "whole," << stride <<","<< elapsed.count() << std::endl;
      // }
      
      // /* measure only convert()
      // std::cerr << "TEST: convert() speed!" << std::endl;
      // auto start = std::chrono::high_resolution_clock::now();
      // KmerCreater kk(1, k, 1111);
      // for(int i = 0; i < 1000000; i++){
      //   keyT first;
      //   kk.convert(read1, first);
      // }
      // auto end = std::chrono::high_resolution_clock::now();
      // std::chrono::duration<double> elapsed = end - start;
      // std::cerr << "conv_fun_winit," << 1 <<","<< elapsed.count() << std::endl;
      // }*/
      // // sheet2
      // std::cerr << "TEST: whole kmer vs concat kmer speed!" << std::endl;
      // for (int stride = 1; stride < 22; stride += 2) {
      //   KmerCreater kk(stride, k, 1111);
      //   auto start = std::chrono::high_resolution_clock::now();

      //   for (int i = 0; i < 1000000; i++) {
      //     keyT first;
      //     keyT last;
      //     kk.convert(read1, first);
      //     int pos = 0;
      //     while (pos + k + stride <= len) {
      //       last = kk.NextKey(first, read1, pos);
      //     }
      //   }
      //   auto end = std::chrono::high_resolution_clock::now();
      //   std::chrono::duration<double> elapsed = end - start;
      //   std::cerr << "concat," << stride << "," << elapsed.count() << std::endl;
      // }
      // /*
      // for(int stride = 1; stride < 22; stride+=2){
      //   auto start = std::chrono::high_resolution_clock::now();

      //   for(int i = 0; i < 1000000; i++){
      //     KmerCreater kk(stride, k, 1111);
      //     keyT first;
      //     kk.convert(read1, first);
      //     int pos = 0;
      //     while(pos + k + stride <= len){
      //       pos = pos + stride;
      //       kct.convert(read1+pos,first);
      //     }
      //   }
      //   auto end = std::chrono::high_resolution_clock::now();
      //   std::chrono::duration<double> elapsed = end - start;
      //   std::cerr << "whole," << stride <<","<< elapsed.count() << std::endl;
      // }*/
      // /* test move
      // std::cerr << "TEST: move_pos() speed!" << std::endl;
      // auto start = std::chrono::high_resolution_clock::now();
      // KmerCreater kk(1, k, 1111);
      // for(int i = 0; i < 100000000; i++){
      //   for(int j = 0; j < len; j++){

      //   }
      // }
      // auto end = std::chrono::high_resolution_clock::now();
      // std::chrono::duration<double> elapsed = end - start;
      // std::cerr << "move_pos," << 1 <<","<< elapsed.count() << std::endl;
      // */
    }
  }
}
//  Opts opts;
//  std::vector<std::string> seqs = {argv[1],argv[2]};
//  opts.single_end = false;
//  opts.seq_files = seqs;
//  opts.threads = atoi(argv[3]);
//  // check OthlloIndex
//  opts.k = atoi(argv[4]);
//  opts.stride = atoi(argv[5]);
//  opts.output_failed = atoi(argv[6]);
//  std::stringstream s1(argv[7]);
//  std::stringstream s2(argv[8]);

//  s1 >> std::boolalpha >> opts.all_kmer;
//  s2 >> std::boolalpha >> opts.fusion;
//  if(opts.k == 31){
//    opts.index_file = "/scratch/lji226/projects/genecount/cc/31/hg38_rep3_k31.bin";
//    opts.geneids_file = "/scratch/lji226/projects/genecount/cc/k21_maxrep3/geneIDs.txt";
//    opts.cliques_file = "/scratch/lji226/projects/genecount/cc/31/GrpMap.txt";
//  }else{
//    opts.index_file =
//      "/scratch/lji226/projects/genecount/cc/k21_maxrep3/grch38_genes_maxRep3_k21.bin";
//    opts.geneids_file =
//      "/scratch/lji226/projects/genecount/cc/k21_maxrep3/geneIDs.txt";
//    opts.cliques_file =
//      "/scratch/lji226/projects/genecount/cc/k21_maxrep3/GrpMap.txt";
//  }

//  std::cerr << "Index: " << opts.index_file << std::endl;

//  OthelloIndex othIndex(opts);
//  othIndex.loadIndex(opts);
//  std::cerr << "Total gene in index: " << othIndex.num_genes << std::endl;
//  std::cerr << "Clique start: " << othIndex.cliqs_begin << std::endl;
//  std::cerr << "Clique end: " << othIndex.cliqs_end << std::endl;
//  GeneCounter gc(othIndex,opts);
//  GeneCounter gcr(othIndex,opts);
//  MasterProcessor MP(othIndex,gc,gcr,opts);
//  //ReadProcessor RP(othIndex, opts, MP);
//  MP.processReads();

// }



// Test single program
// int main(int argc, char **argv){
//  Opts opts;
//  std::vector<std::string> seqs = {"/scratch/lji226/projects/genecount/cc/k21_maxrep3/bug.r1.fq","/scratch/lji226/projects/genecount/cc/k21_maxrep3/bug.r2.fq"};
//  opts.single_end = false;
//  opts.seq_files = seqs;

//  // check OthlloIndex
//  opts.index_file =
//    "/scratch/lji226/projects/genecount/cc/k21_maxrep3/grch38_genes_maxRep3_k21.bin";
//  opts.geneids_file =
//    "/scratch/lji226/projects/genecount/cc/k21_maxrep3/geneIDs.txt";
//  opts.cliques_file =
//    "/scratch/lji226/projects/genecount/cc/k21_maxrep3/GrpMap.txt";
//  opts.fusion = true;

//  OthelloIndex othIndex(opts);
//  othIndex.loadIndex(opts);
//  GeneCounter gc(othIndex,opts);
//  MasterProcessor MP(othIndex,gc,opts);
//  ReadProcessor RP(othIndex, opts, MP);

//  //TGCTGGTCTGCAAGTCAGAGTCCGTGCCACCTGTCACTGACTGGGCCTGGTACAAGATCACTGACTCTGAGGACAAG*GATGAGTTCCACCCGTTCATCGAGGCACTGCTGCCTCACGTCCGCGCTTTCTCCTACACCTGGTTCAACCTGCA
//  char *read1 = "TACAGATGCCCGCCACCACGCCGGGCTGATTTTTGTATTTTTAGTAGAGACGAGGTTTTGCCATGTTAGCCAGGCTGGTCTCTAACTCCCAGCCTCAAGT\0";
//  // read2 is repetitive
//  char *read2 = "CCGGGGGAGGGGGCATGGGGAGGGCAGTATGCTTTCAAAAACCCCTCACAGGCCAGGCGTGGTGGCTCATGCCTGTAATCACAGGACTTTGGGAGGCCGA\0";
//  int rlen = strlen(read1);
//  std::vector<std::pair<valueT,int>> r1;
//  std::vector<std::pair<valueT,int>> r2;
//  othIndex.match(read1,rlen,r1);
//  //othIndex.match(read2,rlen,r2);

//  // std::vector<std::pair<valueT,int>> results;
//  // for(int i=0; i< othIndex.num_genes; i++){
//  //  results.push_back({i+1,0});
//  // }

//  // for(int i = 0; i < r2.size(); i++){
//  //  std::cout << r2[i].first << '\t' << r2[i].second ;
//  //  std::cout << "\t" ;
//  //  for (int j=0; j < i; j++){
//  //    printf(" ");
//  //  }
//  //  printf("%.*s",21,read2+i);
//  //  std::cout << std::endl;
//  //  // std::cout << q2[i].first << '\t';
//  //  // std::cout << q3[i].first << "\t" << q1[i].second <<  std::endl;
//  // }

//  RP.printKmer(r1,read1,rlen);
//  //RP.printKmer(r2,read2,rlen);
//  std::vector<GeneLog> gr1 = RP.Teeth(read1, rlen, 21);
//  //std::vector<GeneLog> gr2 = RP.Teeth(read2, rlen, 21);

//  if(gr1.size()!=0){
//  printf("Read1: tot= %d, value = %d, mb = %d, fpos = %d, lpos = %d\n",  gr1[0].tot,
//      gr1[0].gv,
//      gr1[0].maxBlock,
//      gr1[0].first_p,
//      gr1[0].last_p);}
//  // if(gr2.size()!=0){
//  //  printf("Read2: tot= %d, value = %d, mb = %d, fpos = %d, lpos = %d\n",  gr2[0].tot,
//  //    gr2[0].gv,
//  //    gr2[0].maxBlock,
//  //    gr2[0].first_p,
//  //    gr2[0].last_p);
//  // }
//  // assign reads

//  // if(opts.fusion){
//  //  printf("Start fusion detection\n");
//  //  std::vector<GeneLog> fusion;
//  //  fusionScan(fusion,gr1);
//  //  if(!opts.single_end){
//  //    fusionScan(fusion,gr2);
//  //  }
//  //  bool emit=true;
//  //  if(fusion.size()<2){
//  //    printf("Not fusion!\n");
//  //  }else{

//  //  }

//  // }




// }

