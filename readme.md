FastCount:  A Fast Gene Count Software for Single-cell RNA-seq Data

FastCount is a light-weight *k*-mer based UMI counting algorithm, FastCount, to efficiently quantify UMI counts from single cell RNA-seq data. We demonstrate FastCount is over an order of magnitude faster than Cell Ranger while achieves competitive accuracy on 10X Genomics single cell RNA-seq data.

## Usage

### Installation

```bash
git clone https://merckey@bitbucket.org/merckey/fastcount.git
cd fastcount
make
```

### Build FastCount GeneOthello Index
Before gene counting, a `GeneOthello` index need to be constructed using the reference transcriptome. The `index` function extract all the *k*-mers present in the reference transcript sequences of each gene. These *k*-mers are divided into two categories: gene-specific  and gene-clique *k*-mers. Each of the gene-specific *k*-mers can uniquely identify one gene. Each of *k*-mers in a gene clique set is not unique to a gene and instead, is shared by a set of genes, which we refer to as a gene clique.  The mapping between *k*-mers and the genes features are stored using the `Othello` data structure. 

Inputs:

  - reference transcriptome fasta file
  - `-i`: the output prefix for the index
  - `-k`: the *k*-mer length
  - `-t`: the selected list of transcript ids


```bash
./FastCount index -i index_prefix -k kmer_length -t transcript_id.tx reference_transcriptome.fasta
```

### UMI-based gene count summarization
FastCount performs gene count summarization by first assigning reads to the potential gene feature and then handling the Cell Barcode and UMI information in the sequencing reads to computationally allocate reads to the cells of origin while identifying technical artifacts from PCR amplification.

Inputs:

  - `-i`: the prefix for the contructed GeneOthello Index
  - `--paired`: process both read1 and read2 sequence
  - `k`: the *k*-mer length, it should be the same as the value used in the GeneOthello Index
  - `s`: moving window size 
  - `--scRNA`: process single-cell RNA-seq data
  - `-u`: the UMI sequence length defined in the library protocol
  - `-b`: the cell barcode sequence length defined in the library protocol
  - `-t`: the number of threads 
  - `-w`: the cell barcode whitelist provided by the library protocol

Output:

  - a feature-barcode matrix in Market Exchange Format

```bash
./FastCount count \
-i index_prefix \
--paired \
-k kmer_length \
-s kmer_window_size \
--scRNA \
-u UMI_sequence_length \
-b barcode_sequence_length \
-t num_threads --scRNA \
-w whitelist \
read1.fastq.gz \
read2.fastq.gz
```

